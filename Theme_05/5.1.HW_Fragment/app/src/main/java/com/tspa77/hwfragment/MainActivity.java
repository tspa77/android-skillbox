package com.tspa77.hwfragment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private static final String COUNTER = "counter";
    private int counter = 0;    // Счётчик для фрагментов

    private Button mButtonNewFragment;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Сохраняем сосояние счётчика
        outState.putInt(COUNTER, counter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Связываем объект с кнопкой "Новый фрагмент"
        mButtonNewFragment = findViewById(R.id.button_new_fragment);
        // Назначаем обработчик событий на кнопку "Новый фрагмент"
        mButtonNewFragment.setOnClickListener(v -> {
            addNewFragment();
        });
        // Если нет сохранённых состояний, т.е. первый запуск, то добавляем новый фрагмент
        if (savedInstanceState == null) {
            addNewFragment();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Если есть сохранённые состояния, восстанавливаем значения счётчика
        if (savedInstanceState != null) {
            counter = savedInstanceState.getInt(COUNTER, 0);
        }
    }

    private void addNewFragment() {
        // Добавление нового фрагмента в активити
        getSupportFragmentManager()         // Подключаемся к фрагмент-менеджеру
                .beginTransaction()         // Начинаем транзакцию
                .addToBackStack(null)       // Добавляем фрагмент в бэкстэк
                .replace(R.id.view_container, NewFragment.newInstance(counter))     // Заменяем текущий контйнер на новый фрагмент, который создаём присвоив ему уникальный номер - "счётчик"
                .commit();                  // Завершаем транзакцию
        counter++; // Увеличваем счётчик на единицу
    }

    @Override
    public void onBackPressed() {
        // Раз самый первый фрагмент создан не пользователем а автоматически, то не даём его убить
        // по нажатию бэка , а целиком выходим из приложения
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            getSupportFragmentManager().popBackStack();
        }

        // Убавляем счётчик после уничтожения фрагмента по нажатию на клавишу "бэк"
        if (counter > 0) {
            counter--;
        }
    }
}
