package com.tspa77.hwfragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class NewFragment extends Fragment {
    private int fragmentNumber;

    private TextView mTextView;

    // Создание нового экземпляра фрагмента с полученным индивидуальным номером
    public static NewFragment newInstance(int fragmentNumber) {
        final NewFragment fragment = new NewFragment();
        fragment.fragmentNumber = fragmentNumber;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Если есть сохранённые состояния, восстанавливаем номер фрагмента
        if (savedInstanceState != null) {
            fragmentNumber = savedInstanceState.getInt("fragmentNumber");
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        // Сохраняем текущее состояние, т.е. номер текущего фрагмента
        outState.putInt("fragmentNumber", fragmentNumber);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Создаём вью и подгружаем туда макет/разметку
        final View view = inflater.inflate(R.layout.fragment_new, container, false);
        // Связываем объект с текстовым полем
        mTextView = view.findViewById(R.id.textView);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Устанавливаем текст для текстового поля
        mTextView.setText("Фрагмент № " + fragmentNumber);
    }
}


