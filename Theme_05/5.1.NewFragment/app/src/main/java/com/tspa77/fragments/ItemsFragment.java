package com.tspa77.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class ItemsFragment extends Fragment {

    private static final String TAG = "MyDebugItemsFragment";

    private Listener mListener;
    private Button mItem1Button;
    private Button mItem2Button;

    public interface Listener {
        void onItem1Click(ItemsFragment fragment);
        void onItem2Click(ItemsFragment fragment);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (Listener) getActivity();
        Log.d(TAG, "onAttach");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        return inflater.inflate(R.layout.fragment_items, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mItem1Button = view.findViewById(R.id.view_item1_button);
        mItem2Button = view.findViewById(R.id.view_item2_button);

        mItem1Button.setOnClickListener(v -> {
            mListener.onItem1Click(ItemsFragment.this);
        });
        mItem2Button.setOnClickListener(v -> {
            mListener.onItem2Click(ItemsFragment.this);
        });
        Log.d(TAG, "onViewCreated");
    }


}
