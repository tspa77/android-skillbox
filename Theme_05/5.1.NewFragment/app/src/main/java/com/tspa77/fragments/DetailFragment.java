package com.tspa77.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DetailFragment extends Fragment {
    private static final String TAG = "MyDebugDetailFragment";
    public static final String NAME_KEY = "DetailFragment.NAME";

    private TextView mNameTextView;


    public static DetailFragment newInstance(final String name){
        final DetailFragment fragment = new DetailFragment();
        final Bundle arguments = new Bundle();
        arguments.putString(NAME_KEY, name);
        fragment.setArguments(arguments);
        Log.d(TAG, "create newInstance");
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_detail, container, false);
        mNameTextView = view.findViewById(R.id.view_name);
        Log.d(TAG, "onCreateView");
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState==null){
            mNameTextView.setText(getArguments().getString(NAME_KEY));
        } else {
            mNameTextView.setText(savedInstanceState.getString(NAME_KEY));
        }

        Log.d(TAG, "onActivityCreated");
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(NAME_KEY, mNameTextView.getText().toString());
    }

    public void setName(final String name){
        mNameTextView.setText(name);

    }
}