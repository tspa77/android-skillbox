package com.tspa77.fragments;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

public class MainActivity extends AppCompatActivity implements ItemsFragment.Listener {
    private static final String TAG = "MyDebugMainActivity";
    private ViewGroup mItemsListContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mItemsListContainer = findViewById(R.id.view_items);

        if (savedInstanceState == null) {
            if (isTablet()) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.view_items, new ItemsFragment())
                        .replace(R.id.view_container, new DetailFragment().newInstance("N/A"))
                        .commit();
            } else {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.view_container, new ItemsFragment())
                        .commit();
            }
        }
        Log.d(TAG, "onCreate");
    }


    // onItem1Click - через создание нового фрагмента
    @Override
    public void onItem1Click(ItemsFragment fragment) {
        final String name = "Item 1";

        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.view_container, DetailFragment.newInstance(name))
                .commit();
    }

    // onItem2Click - первый иф через обновление имеющегося фрагмента
    @Override
    public void onItem2Click(ItemsFragment fragment) {
        final String name = "Item 2";
        if (isTablet()) {
            final DetailFragment detailFragment
                    = (DetailFragment) getSupportFragmentManager().findFragmentById(R.id.view_container);
            detailFragment.setName(name);
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.view_container, DetailFragment.newInstance("Item 2"))
                    .commit();
        }
    }

    private boolean isTablet() {
        return mItemsListContainer != null;
    }
}
