package com.tspa77.myapplication;

/** Мой логгер с настроенным под себя отображением */

import android.util.Log;

public class Logger {
    private static final String TAG = "***";

    public static void log(Object object){
        Log.d(TAG, "\t\t***\t\t " +  object.getClass().getSimpleName()
                + " (" + object.hashCode() + ") " + "\t-->\t\t"
                + Thread.currentThread().getStackTrace()[3].getMethodName());
    }
}
