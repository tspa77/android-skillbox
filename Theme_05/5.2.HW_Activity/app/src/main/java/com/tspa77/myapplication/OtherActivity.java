package com.tspa77.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

import static com.tspa77.myapplication.Logger.log;

public class OtherActivity extends AppCompatActivity {
    private static final String RANDOM_NUMBER_KEY = "OtherActivity random number";

    private final Random mRandom = new Random();

    private Button mButton;
    private TextView mTextView;

    int number = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other);
        log(this);

        // Связываем объект с кнопкой
        mButton = findViewById(R.id.button_number_generator);
        // Связываем объект с текстовым полем
        mTextView = findViewById(R.id.textview_number);
        // Назначаем обработчик событий на кнопку
        mButton.setOnClickListener(v -> {
            generateNumber(); // вызываем метод
        });

    }

    private void generateNumber() {
        log(this);
        // метод генерирующий случайное число и заносящий его в текстовое поле
        mTextView.setText(Integer.toString(mRandom.nextInt(100)));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        log(this);

        // Если текстовое поле не пустое, сохранияем значение
        if (mTextView.getText() != null) {
            outState.putString(RANDOM_NUMBER_KEY, mTextView.getText().toString());
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        log(this);

        // Если есть сохранённые состояния и поле не пустое, восстанавливаем хранимое число
        // Проверка на непустое поле - при запуске активити поле пустое, но если начнём вращать, то
        // savedInstanceState будет не налл, но восстанавливать всё павно нечего, т.к. ещё не
        // генерировали номер. Если номер сгенерили, то он сохраянется и восстанавливается при вращении
        if (savedInstanceState != null && mTextView.getText() != null) {
            mTextView.setText(savedInstanceState.getString(RANDOM_NUMBER_KEY));
        }
    }
}
