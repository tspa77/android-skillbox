package com.tspa77.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import static com.tspa77.myapplication.Logger.log;

public class MainActivity extends AppCompatActivity {

    Button mButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        log(this);

        // Связываем объект с кнопкой
        mButton = findViewById(R.id.button_next_activity);
        // Назначаем обработчик событий на кнопку
        mButton.setOnClickListener(v -> {
            // Запускаем новую актвити, с параметрами укзанными в интенте.
            startActivity(new Intent(this, OtherActivity.class));
        });
    }
}
