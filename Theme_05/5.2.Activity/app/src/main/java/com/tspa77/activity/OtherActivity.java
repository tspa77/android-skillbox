package com.tspa77.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import static com.tspa77.activity.Logger.log;

public class OtherActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other);

        log(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        log(this);
    }


    @Override
    protected void onResume() {
        super.onResume();

        log(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        log(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        log(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        log(this);
    }

    public void onButtonClick(View view) {
        startActivity(new Intent(this, MainActivity.class));

        log(this);
    }
}
