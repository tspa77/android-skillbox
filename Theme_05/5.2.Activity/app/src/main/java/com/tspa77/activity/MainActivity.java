package com.tspa77.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

import static com.tspa77.activity.Logger.log;

public class MainActivity extends AppCompatActivity {
    private static final String RANDOM_NUMBER_KEY = "MainActivityRandom";
    private static final String USER_NUMBER_KEY = "MainActivityUser";

    private final Random mRandom = new Random();

    private Button mButton;
    private EditText mEditText;
    private TextView mTextView;

    private int mRandomNumber;
    private int mUserNumber;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mButton = findViewById(R.id.view_button);
        mEditText = findViewById(R.id.view_user_number);
        mTextView = findViewById(R.id.view_sum);

        mButton.setOnClickListener(v -> {
            mRandomNumber = mRandom.nextInt(100);
            mUserNumber = Integer.parseInt(mEditText.getText().toString());
            mTextView.setText(String.format("%d", mRandomNumber + mUserNumber));
        });

        if (savedInstanceState != null) {
            mRandomNumber = savedInstanceState.getInt(RANDOM_NUMBER_KEY);
            mUserNumber = savedInstanceState.getInt(USER_NUMBER_KEY);
            mTextView.setText(String.format("%d", mRandomNumber + mUserNumber));
        }

        log(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(RANDOM_NUMBER_KEY, mRandomNumber);
        outState.putInt(USER_NUMBER_KEY, mUserNumber);
    }

    @Override
    protected void onStart() {
        super.onStart();

        log(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        log(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        log(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        log(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        log(this);
    }

    public void onButtonClick(View view) {
        startActivity(new Intent(this, OtherActivity.class));

        log(this);
    }


}
