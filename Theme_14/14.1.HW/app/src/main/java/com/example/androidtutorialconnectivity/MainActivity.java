package com.example.androidtutorialconnectivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private ConnectivityManager mConnectivityManager;

    private final BroadcastReceiver mConnectivityReceiver = new ConnectivityReceiver();
    private TextView txtViewStatus;
    private ImageView imageViewStatus;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mConnectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

        registerReceiver(mConnectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterReceiver(mConnectivityReceiver);
    }

    private void logConnectivity(final NetworkInfo networkInfo) {
        final String state;
        if (networkInfo != null && networkInfo.getState() == NetworkInfo.State.CONNECTED) {
            state = "Connected";
            txtViewStatus.setText(R.string.status_connected);
            imageViewStatus.setImageResource(R.drawable.ic_signal_cellular_4_bar_darkgrey_48dp);

        } else {
            state = "Not connected";
            txtViewStatus.setText(R.string.status_not_connected);
            imageViewStatus.setImageResource(R.drawable.ic_signal_cellular_off_darkgrey_48dp);
        }

        Log.d("!@#", "Network: " + state);
    }

    private void initView() {
        txtViewStatus = findViewById(R.id.txtView_status);
        imageViewStatus = findViewById(R.id.imageView_status);
    }

    private class ConnectivityReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {
            logConnectivity(mConnectivityManager.getActiveNetworkInfo());
        }
    }


}
