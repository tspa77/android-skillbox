package com.example.androidtutorialconnectivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

	private ConnectivityManager mConnectivityManager;

	private final BroadcastReceiver mConnectivityReceiver = new ConnectivityReceiver();

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mConnectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

		registerReceiver(mConnectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		unregisterReceiver(mConnectivityReceiver);
	}

	private void logConnectivity(final NetworkInfo networkInfo) {
		final String state;
		if (networkInfo != null && networkInfo.getState() ==  NetworkInfo.State.CONNECTED) {
			state = "Connected";
		} else {
			state = "Not connected";
		}

		Log.d("!@#", "Network: " + state);
	}

	private class ConnectivityReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(final Context context, final Intent intent) {
			logConnectivity(mConnectivityManager.getActiveNetworkInfo());
		}
	}
}
