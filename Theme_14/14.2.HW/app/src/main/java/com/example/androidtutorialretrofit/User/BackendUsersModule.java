package com.example.androidtutorialretrofit.User;

import com.example.androidtutorialretrofit.User.UsersPOJO.Result;
import com.example.androidtutorialretrofit.User.UsersPOJO.ResultsUserAPI;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.androidtutorialretrofit.Logger.log;

public class BackendUsersModule {
    private static BackendUsersModule sInstance;

    private final Retrofit mUserRetrofit;
    private final BackendUsers mBackendUsers;

    private UserState mUserUserState = UserState.USER_IDLE;
    private ResultsUserAPI resultsUserAPI;
    private List<Result> mUsers = new ArrayList<>();

    private UserListener mUserListener;

    public static BackendUsersModule getInstance() {
        if (sInstance == null) {
            synchronized (BackendUsersModule.class) {
                if (sInstance == null) {
                    sInstance = new BackendUsersModule();
                }
            }
        }
        return sInstance;
    }

    private BackendUsersModule() {
        log(this);

        mUserRetrofit = new Retrofit.Builder()
                .baseUrl("https://randomuser.me/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mBackendUsers = mUserRetrofit.create(BackendUsers.class);
    }

    private void changeUserState(final UserState newUserState) {
        log(this);
        mUserUserState = newUserState;
        if (mUserListener != null) {
            mUserListener.onUsersStateChanged(mUserUserState);
        }
    }

    public void loadUsers() {
        log(this);
        if (mUserUserState != UserState.USER_IDLE) {
            return;
        }

        changeUserState(UserState.USER_LOADING);
        mBackendUsers.resultAPI().enqueue(new Callback<ResultsUserAPI>() {
            @Override
            public void onResponse(Call<ResultsUserAPI> call, Response<ResultsUserAPI> response) {
                log(this);
                resultsUserAPI = response.body();
                mUsers = resultsUserAPI.getResults();
                if (mUserListener != null) {
                    log(this, String.valueOf(mUsers.size()));
                    mUserListener.onUsersLoaded(mUsers);
                }
                changeUserState(UserState.USER_IDLE);
            }

            @Override
            public void onFailure(Call<ResultsUserAPI> call, Throwable t) {
                log(this, t.getMessage());
                if (mUserListener != null) {
                    mUserListener.onUsersLoadingFailed(t);
                }
                changeUserState(UserState.USER_IDLE);
            }
        });


    }

    public void setListener(final UserListener listener) {
        log(this);
        mUserListener = listener;
        if (mUserListener != null) {
            mUserListener.onUsersStateChanged(mUserUserState);
        }
    }

    public List<Result> getUsers() {
        log(this, mUsers.toString());
        return mUsers;
    }

    public enum UserState {
        USER_IDLE, USER_LOADING
    }

    public interface UserListener {

        void onUsersStateChanged(UserState userState);

        void onUsersLoaded(List<Result> results);

        void onUsersLoadingFailed(Throwable t);
    }
}
