package com.example.androidtutorialretrofit.Post;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface BackendPosts {

	@GET("/posts")
	Call<List<Post>> listPosts();
}
