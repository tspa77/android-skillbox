package com.example.androidtutorialretrofit.Post;

import com.google.gson.annotations.SerializedName;

public class Post {

	@SerializedName("userId")
	long mUserId;

	@SerializedName("id")
	long mId;

	@SerializedName("title")
	String mTitle;

	@SerializedName("body")
	String mBody;

	public Post(final long userId, final long id, final String title, final String body) {
		mUserId = userId;
		mId = id;
		mTitle = title;
		mBody = body;
	}

    public long getmUserId() {
        return mUserId;
    }

    public long getmId() {
        return mId;
    }

    public String getmTitle() {
        return mTitle;
    }

    public String getmBody() {
        return mBody;
    }
}
