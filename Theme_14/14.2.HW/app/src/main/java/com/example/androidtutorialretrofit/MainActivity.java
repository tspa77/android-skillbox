package com.example.androidtutorialretrofit;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.androidtutorialretrofit.Post.BackendPostsModule;
import com.example.androidtutorialretrofit.Post.Post;
import com.example.androidtutorialretrofit.User.BackendUsersModule;
import com.example.androidtutorialretrofit.User.UsersPOJO.Result;

import java.util.List;

import static com.example.androidtutorialretrofit.Logger.log;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private final BackendPostsModule mBackendPostsModule = BackendPostsModule.getInstance();
    private final BackendUsersModule mBackendUsersModule = BackendUsersModule.getInstance();

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private OverallAdapter overallAdapter;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        log(this);

        mSwipeRefreshLayout = findViewById(R.id.view_swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mBackendPostsModule.loadPosts();
            mBackendUsersModule.loadUsers();
        });

        overallAdapter = new OverallAdapter();
        mRecyclerView = findViewById(R.id.recyclerview);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(overallAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        log(this);

        mBackendPostsModule.setListener(mPostPostListener);
        mBackendUsersModule.setListener(mUserListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        log(this);

        mBackendPostsModule.setListener(null);
        mBackendUsersModule.setListener(null);
    }


    private final BackendPostsModule.PostListener mPostPostListener = new BackendPostsModule.PostListener() {
        @Override
        public void onPostStateChanged(final BackendPostsModule.PostState postState) {
            log(this);
            switch (postState) {
                case POST_IDLE:
                    log(this, "POST_IDLE");
                    mSwipeRefreshLayout.setRefreshing(false);
                    mRecyclerView.setVisibility(View.VISIBLE);
                    overallAdapter.setPosts(mBackendPostsModule.getPosts());
                    break;
                case POST_LOADING:
                    log(this, "POST_LOADING");
                    mRecyclerView.setVisibility(View.GONE);
                    mSwipeRefreshLayout.setRefreshing(true);
                    break;
            }
        }

        @Override
        public void onPostsLoaded(final List<Post> posts) {
            log(this);
            // do nothing
        }

        @Override
        public void onPostsLoadingFailed(final Throwable t) {
            Toast.makeText(MainActivity.this, "Error loading posts", Toast.LENGTH_SHORT).show();
            Log.e(TAG, "Error loading posts" + t.getMessage(), t);
        }
    };


    private final BackendUsersModule.UserListener mUserListener = new BackendUsersModule.UserListener() {
        @Override
        public void onUsersStateChanged(final BackendUsersModule.UserState userState) {
            log(this);
            switch (userState) {
                case USER_IDLE:
                    log(this, "USER_IDLE");
                    overallAdapter.setUsers(mBackendUsersModule.getUsers());
                    break;
                case USER_LOADING:
                    log(this, "USER_LOADING");
                    break;
            }
        }

        @Override
        public void onUsersLoaded(List<Result> results) {
            // do nothing
            log(this);
        }

        @Override
        public void onUsersLoadingFailed(final Throwable t) {
            log(this);
            Toast.makeText(MainActivity.this, "Error loading users", Toast.LENGTH_SHORT).show();
            Log.e(TAG, "Error loading users", t);
        }
    };
}
