package com.example.androidtutorialretrofit.User;

import com.example.androidtutorialretrofit.User.UsersPOJO.ResultsUserAPI;

import retrofit2.Call;
import retrofit2.http.GET;

public interface BackendUsers {
    @GET("?inc=name,email,picture&noinfo&nat=AU, BR, CA, CH, DE, DK, ES, FI, FR, GB, IE, NO, US&results=100")
    Call<ResultsUserAPI> resultAPI();
}


