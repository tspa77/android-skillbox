package com.example.androidtutorialretrofit.Post;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.androidtutorialretrofit.Logger.log;

public class BackendPostsModule {
    private static BackendPostsModule sInstance;

    private final Retrofit mPostRetrofit;
    private final BackendPosts mBackendPosts;

    private PostState mPostState = PostState.POST_IDLE;
    private List<Post> mPosts = new ArrayList<>();

    private PostListener mPostListener;


    public static BackendPostsModule getInstance() {
        if (sInstance == null) {
            synchronized (BackendPostsModule.class) {
                if (sInstance == null) {
                    sInstance = new BackendPostsModule();
                }
            }
        }
        return sInstance;
    }

    private BackendPostsModule() {
        log(this);
        mPostRetrofit = new Retrofit.Builder()
                .baseUrl("http://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mBackendPosts = mPostRetrofit.create(BackendPosts.class);
        log(this, String.valueOf(mBackendPosts));
    }

    private void changePostState(final PostState newPostState) {
        log(this);
        mPostState = newPostState;

        if (mPostListener != null) {
            mPostListener.onPostStateChanged(mPostState);
        }
    }

    public void loadPosts() {
        log(this);
        if (mPostState != PostState.POST_IDLE) {
            return;
        }

        changePostState(PostState.POST_LOADING);
        // получаем объект Call (mBackendPosts.listPosts()) и ?ставим в очередь? коллбэк который
        // вернёт результат (onResponse) либо ошибку (onFailure)
        mBackendPosts.listPosts().enqueue(new Callback<List<Post>>() {

            @Override
            public void onResponse(final Call<List<Post>> call, final Response<List<Post>> response) {
                log(this);
                mPosts = response.body();
                log(this, String.valueOf(mPosts.size()));
                if (mPostListener != null) {
                    mPostListener.onPostsLoaded(response.body());
                }
                changePostState(PostState.POST_IDLE);
            }

            @Override
            public void onFailure(final Call<List<Post>> call, final Throwable t) {
                log(this);
                if (mPostListener != null) {
                    mPostListener.onPostsLoadingFailed(t);
                }
                changePostState(PostState.POST_IDLE);
            }
        });
    }

    public void setListener(final PostListener postListener) {
        log(this);
        mPostListener = postListener;
        if (mPostListener != null) {
            mPostListener.onPostStateChanged(mPostState);
        }
    }

    public List<Post> getPosts() {
        log(this);
        return mPosts;
    }

    public enum PostState {
        POST_IDLE, POST_LOADING
    }

    public interface PostListener {

        void onPostStateChanged(PostState postState);

        void onPostsLoaded(List<Post> posts);

        void onPostsLoadingFailed(Throwable t);
    }
}
