package com.example.androidtutorialretrofit;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.androidtutorialretrofit.Post.Post;
import com.example.androidtutorialretrofit.User.UsersPOJO.Result;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.example.androidtutorialretrofit.Logger.log;

public class OverallAdapter extends RecyclerView.Adapter<OverallAdapter.ViewHolder> {
    private List<Post> mPosts = new ArrayList<>();
    private List<Result> mUsers = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        log(this);
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recyclerview_card, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        log(this);
        viewHolder.textViewUserPost.setText(mPosts.get(i).getmTitle());
        int userId = (int) mPosts.get(i).getmUserId();
        try {
            log(this, String.valueOf(mUsers.size()) + "  Size of mUser   ");
            String name = mUsers.get(userId).getName().getFirst().concat(" ").concat(mUsers.get(userId).getName().getLast());
            viewHolder.textViewUserName.setText(name);
            viewHolder.textViewUserEmail.setText(mUsers.get(userId).getEmail());
            String photoURL = mUsers.get(userId).getPicture().getMedium();
            Picasso.get()
                    .load(photoURL)
                    .placeholder(R.drawable.ic_autorenew_grey_72dp)
                    .error(R.drawable.ic_person_outline_grey_72dp)
                    .into(viewHolder.imageViewUserAvatar);
        } catch (IndexOutOfBoundsException e) {
            log(this, e.getMessage());
        }
    }


    @Override
    public int getItemCount() {
        log(this);
        return mPosts.size();
    }


    public void setPosts(final List<Post> posts) {
        log(this);
        // На сайте все посты по порядку, надо перемешать
        List<Post> randomSortedPosts = new ArrayList<>(posts);
        Collections.shuffle(randomSortedPosts);
        mPosts = randomSortedPosts;
        notifyDataSetChanged();
    }


    public void setUsers(final List<Result> Results) {
        log(this);
        mUsers = Results;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public final ImageView imageViewUserAvatar;
        public final TextView textViewUserName, textViewUserEmail, textViewUserPost;

        public ViewHolder(final View itemView) {
            super(itemView);
            log(this);
            imageViewUserAvatar = itemView.findViewById(R.id.imageView_user_avatar);
            textViewUserName = itemView.findViewById(R.id.textView_user_name);
            textViewUserEmail = itemView.findViewById(R.id.textView_user_email);
            textViewUserPost = itemView.findViewById(R.id.textView_user_post);
        }
    }
}
