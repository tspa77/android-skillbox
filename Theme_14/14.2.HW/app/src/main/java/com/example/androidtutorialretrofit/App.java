package com.example.androidtutorialretrofit;

import android.app.Application;

import com.example.androidtutorialretrofit.Post.BackendPostsModule;
import com.example.androidtutorialretrofit.User.BackendUsersModule;

public class App extends Application {

	@Override
	public void onCreate() {
		super.onCreate();

		BackendPostsModule.getInstance();
		BackendUsersModule.getInstance();
	}
}
