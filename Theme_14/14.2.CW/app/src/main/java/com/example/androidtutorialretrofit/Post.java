package com.example.androidtutorialretrofit;

import com.google.gson.annotations.SerializedName;

public class Post {

	@SerializedName("userId")
	long mUserId;

	@SerializedName("id")
	long mId;

	@SerializedName("title")
	String mTitle;

	@SerializedName("body")
	String mBody;

	public Post(final long userId, final long id, final String title, final String body) {
		mUserId = userId;
		mId = id;
		mTitle = title;
		mBody = body;
	}
}
