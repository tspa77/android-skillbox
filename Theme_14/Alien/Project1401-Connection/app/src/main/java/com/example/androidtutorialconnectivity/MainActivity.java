// Модуль 14, урок 1: Определение наличия сети.
//          Отображать статус Connectivity в TextView
//
// 35987: Валерий Куликов, 79068017667@yandex.ru
// 2018/11/18
//
//  Примечания:
//      - использовал data binding вместо findViewById для абстрагирования от конкретной вьюхи
//

package com.example.androidtutorialconnectivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.androidtutorialconnectivity.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ConnectivityManager mConnectivityManager;

    private final BroadcastReceiver mConnectivityReceiver = new ConnectivityReceiver();

    private ActivityMainBinding mBinding;

    /**
     * 1. инициализируем биндер,
     * 2. получаем уссылку на системный сервис для запроса статуса соединения
     * 3. регистрируем ресивер для получения ответа от сервиса статуса соединения,
     *      передавая соответствующий фильтр
     */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        mConnectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

        registerReceiver(mConnectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    /**
     * Обнуляем регистрацию при удалении активности
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterReceiver(mConnectivityReceiver);
    }

    /**
     * Отображаем состояние соединения в поле активности используя привязки.
     * @param networkInfo статус сети
     */
    private void showConnectivityState(final NetworkInfo networkInfo) {
        mBinding.setConnectionState(
                getResources().getString(
                        networkInfo != null && networkInfo.getState() == NetworkInfo.State.CONNECTED ?
                                R.string.msg_connected : R.string.msg_not_connected
                )
        );
    }

    /**
     * Класс-приемник данных, отправленных широковещательными сервисами.
     */
    private class ConnectivityReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            showConnectivityState(mConnectivityManager.getActiveNetworkInfo());
        }
    }
}
