package com.example.androidtutorialretrofit;

import android.app.Application;

public class App extends Application {

	@Override
	public void onCreate() {
		super.onCreate();

		// Постройка синглтона
		BackendModule.createInstance();
	}
}
