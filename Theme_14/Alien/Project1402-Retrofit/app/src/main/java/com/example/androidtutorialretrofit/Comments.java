package com.example.androidtutorialretrofit;

import com.google.gson.annotations.SerializedName;

/**
 * POJO класс для маппирования JSON объекта в Java-сущность.
 */
public class Comments {

	@SerializedName("postId")
	final long postId;

	@SerializedName("id")
	final long id;

	@SerializedName("name")
	final String name;

	@SerializedName("email")
	final String email;

	@SerializedName("body")
	public String body;

	public Comments(long postId, long id, String name, String email, String body) {
		this.postId = postId;
		this.id = id;
		this.name = name;
		this.email = email;
		this.body = body;
	}
}
