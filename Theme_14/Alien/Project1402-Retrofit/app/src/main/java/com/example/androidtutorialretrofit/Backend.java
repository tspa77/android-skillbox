package com.example.androidtutorialretrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Интерфейс ретрофит-запроса
 */
public interface Backend {

    /**
     * Аннотация GET строит GET запрос с указанными параметрами.
     * @return Интерфейс ретрофит-методов для обработки запросов.
     */
	@GET("/comments")
	Call<List<Comments>> listPosts();
}
