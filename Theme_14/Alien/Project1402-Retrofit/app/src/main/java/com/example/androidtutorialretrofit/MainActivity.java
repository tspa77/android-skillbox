// Модуль 14, урок 2: RetroFit. Загрузка и использование JSON с сервера.
//          Загрузить с тестового сервера и отобразить список Пользователей или список Комментариев,
//          аналогично тому как мы загрузили список Постов
//
// 35987: Валерий Куликов, 79068017667@yandex.ru
// 2018/11/18
//
//  Примечания:
//      - использовал data binding вместо findViewById для абстрагирования от конкретной вьюхи
//      - удалил onCommentsLoaded из интерфейса слушателя, чтобы не множить ненужные сущности
//      - выровнял итемы листа по 3 строки в каждом, чисто для красоты
//

package com.example.androidtutorialretrofit;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.databinding.DataBindingUtil;

import com.example.androidtutorialretrofit.databinding.ListItemBinding;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private final BackendModule mBackendModule = BackendModule.getInstance();

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mPostsList;
    private CommentsAdapter mAdapter;

    /**
     * Листенер для бекенда с обработкой событий.
     */
    private final BackendModule.Listener mListener = new BackendModule.Listener() {

        /**
         * Строим интерфейс активности в зависимости от состояния
         * - IDLE, значит в данный момент бекенд уже получил данные и простаивает.
         *  -- прячем прогресс-бар
         *  -- отображаем список и загружаем данные в адаптер
         * - LOADING, происходит загрузка
         *  -- отображаем прогресс-бар
         *  -- прячем список
         */
        @Override
        public void onStateChanged(final BackendModule.State state) {
            switch (state) {
                case IDLE:
                    mSwipeRefreshLayout.setRefreshing(false);
                    mPostsList.setVisibility(View.VISIBLE);
                    mAdapter.setComments(mBackendModule.getComments());
                    break;

                case LOADING:
                    mPostsList.setVisibility(View.GONE);
                    mSwipeRefreshLayout.setRefreshing(true);
                    break;
            }
        }

        /**
         * Обработка ошибки запроса
         */
        @Override
        public void onCommentsLoadingFailed(final Throwable t) {
            Toast.makeText(MainActivity.this, "Error loading posts", Toast.LENGTH_SHORT).show();
            Log.e(TAG, "Error loading posts", t);
        }
    };

    /**
     * Инициализируем поля и переменные, подключаем коллбек к свайпу, загружаем данные при первой постройке активности.
     */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSwipeRefreshLayout = findViewById(R.id.view_swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                mBackendModule.loadComments();
            }
        });

        mAdapter = new CommentsAdapter();
        mPostsList = findViewById(R.id.view_posts);
        mPostsList.setLayoutManager(new LinearLayoutManager(this));
        mPostsList.setAdapter(mAdapter);
        mPostsList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        // Первая загрузка при создании активности
        if (savedInstanceState == null) {
            mBackendModule.loadComments();
        }
    }

    /**
     * Подключаем листенера к бекенду при восстановлении активности.
     */
    @Override
    protected void onResume() {
        super.onResume();

        mBackendModule.setListener(mListener);
    }

    /**
     * Отключаем листенера при постановке активности в паузу.
     */
    @Override
    protected void onPause() {
        super.onPause();

        mBackendModule.setListener(null);
    }

    /**
     * Адаптер для отображения комментов.
     */
    private class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {

        private List<Comments> mComments = new ArrayList<>();

        /**
         * Биндим макет итема листа и передаем биндинг в конструктор холдера.
         */
        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
            final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            final ListItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.list_item, parent, false);
            return new ViewHolder(binding);
        }

        /**
         * Устанавливаем экземпляр "коммента" в биндер холдера, при этом в макете автоматически применятся данные.
         */
        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
            holder.binding.setComment(mComments.get(position));
        }

        @Override
        public int getItemCount() {
            return mComments.size();
        }

        public void setComments(final List<Comments> comments) {
            mComments = comments;
            notifyDataSetChanged();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            ListItemBinding binding;

            /**
             * Назначаем ссылку на биндер внутрь экземпляра,
             * и устанавливаем некоторые свойства поля text.
             */
            public ViewHolder(@NonNull final ListItemBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
                binding.text.setMaxLines(3);
                binding.text.setEllipsize(TextUtils.TruncateAt.END);
            }
        }
    }
}
