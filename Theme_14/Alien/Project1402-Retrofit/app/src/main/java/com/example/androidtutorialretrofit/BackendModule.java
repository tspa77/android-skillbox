package com.example.androidtutorialretrofit;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Синглтон для инкапсуляции работы с запросами через ретрофит.
 */
public class BackendModule {
	private static BackendModule sInstance;

	private final Retrofit mRetrofit;
	private final Backend mBackend;

	private State mState = State.IDLE;
	private List<Comments> mComments = new ArrayList<>();

	private Listener mListener;

	public static void createInstance() {
		sInstance = new BackendModule();
	}

	public static BackendModule getInstance() {
		return sInstance;
	}

	private BackendModule() {
		mRetrofit = new Retrofit.Builder()
				.baseUrl("http://jsonplaceholder.typicode.com/")
				.addConverterFactory(GsonConverterFactory.create())
				.build();
		mBackend = mRetrofit.create(Backend.class);
	}

	/**
	 * Дергает листенера при изменении состояния бекенда.
	 * @param newState новое состояние.
	 */
	private void changeState(final State newState) {
		mState = newState;

		if (mListener != null) {
			mListener.onStateChanged(mState);
		}
	}

	/**
	 * Команда на загрузку комментариев.
	 * Обрабатывает текущее состояние, формирует асинхронный запрос к ретрофиту, вовзращает результат и состояние.
	 */
	public void loadComments() {
		if (mState != State.IDLE) {
			return;
		}

		changeState(State.LOADING);

        /**
         * Запускаем асинхронный ретрофит-запрос и обрабатываем ответы.
         */
		mBackend.listPosts().enqueue(new Callback<List<Comments>>() {

            /**
             * При положительном ответе заполняем список и сообщаем листенеру об изменении состояния через метод changeState.
             */
			@Override
			public void onResponse(final Call<List<Comments>> call, final Response<List<Comments>> response) {
				mComments = response.body();
				changeState(State.IDLE);
			}

			@Override
			public void onFailure(final Call<List<Comments>> call, final Throwable t) {
				if (mListener != null) {
					mListener.onCommentsLoadingFailed(t);
				}
				changeState(State.IDLE);
			}
		});
	}

    /**
     * Установка листенера
     */
	public void setListener(final Listener listener) {
		mListener = listener;
		if (mListener != null) {
			mListener.onStateChanged(mState);
		}
	}

    /**
     * Возвращает кешированный список комментариев.
     */
	public List<Comments> getComments() {
		return mComments;
	}

    /**
     * Перечень состояний бекенда.
     */
	public enum State {
		IDLE, LOADING
	}

    /**
     * Интерфейс для передачи состояния и данных листенеру.
     */
	public interface Listener {

        /**
         * В случае изменения состояния.
         */
		void onStateChanged(State state);

        /**
         * В случае ошибки.
         */
		void onCommentsLoadingFailed(Throwable t);
	}
}
