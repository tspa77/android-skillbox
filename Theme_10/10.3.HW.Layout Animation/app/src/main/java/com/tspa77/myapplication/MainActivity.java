package com.tspa77.myapplication;

import android.animation.LayoutTransition;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.view.ViewGroup;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private final Random mRandom = new Random();

    View viewAdd, viewRemove;
    SwitchCompat switchAnimationHorizontal, switchAnimationVertical;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ViewGroup list_vertical = findViewById(R.id.view_list_vertical);
        final ViewGroup list_horizontal = findViewById(R.id.view_list_horizontal);

        final ViewGroup.LayoutParams layoutParams_vertical = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 100);
        final ViewGroup.LayoutParams layoutParams_horizontal = new ViewGroup.LayoutParams(100, ViewGroup.LayoutParams.MATCH_PARENT);


        viewAdd = findViewById(R.id.view_add);
        viewRemove = findViewById(R.id.view_remove);
        switchAnimationHorizontal = findViewById(R.id.switch_animation_horizontal);
        switchAnimationVertical = findViewById(R.id.switch_animation_vertical);

        viewAdd.setOnClickListener(v -> {
            addView(list_vertical, layoutParams_vertical, list_horizontal, layoutParams_horizontal);
        });

        viewRemove.setOnClickListener(v -> {
            removeView(list_vertical, list_horizontal);
        });

        switchAnimationHorizontal.setOnCheckedChangeListener((buttonView, isChecked) -> {
            switchAnimation_horizontal(isChecked);
        });

        switchAnimationVertical.setOnCheckedChangeListener((buttonView, isChecked) -> {
            switchAnimation_vertical(isChecked);
        });
    }


    private void switchAnimation_vertical(boolean isChecked) {
        if (isChecked) {
            ((ViewGroup) findViewById(R.id.view_list_vertical)).setLayoutTransition(new LayoutTransition());
        } else {
            ((ViewGroup) findViewById(R.id.view_list_vertical)).setLayoutTransition(null);
        }
    }


    private void switchAnimation_horizontal(boolean isChecked) {
        if (isChecked) {
            ((ViewGroup) findViewById(R.id.view_list_horizontal)).setLayoutTransition(new LayoutTransition());
        } else {
            ((ViewGroup) findViewById(R.id.view_list_horizontal)).setLayoutTransition(null);
        }
    }


    private void removeView(ViewGroup list_vertical, ViewGroup list_horizontal) {
        final int childCount = list_vertical.getChildCount();
        if (childCount > 0) {
            list_vertical.removeViewAt((int) (mRandom.nextDouble() * childCount));
            list_horizontal.removeViewAt((int) (mRandom.nextDouble() * childCount));
        }
    }


    private void addView(ViewGroup list_vertical, ViewGroup.LayoutParams layoutParams_vertical,
                         ViewGroup list_horizontal, ViewGroup.LayoutParams layoutParams_horizontal) {

        final View view_vertical = new View(MainActivity.this);
        final View view_horizontal = new View(MainActivity.this);

        view_vertical.setLayoutParams(layoutParams_vertical);
        view_vertical.setBackgroundColor(randomColor());
        list_vertical.addView(view_vertical);

        view_horizontal.setLayoutParams(layoutParams_horizontal);
        view_horizontal.setBackgroundColor(randomColor());
        list_horizontal.addView(view_horizontal);
    }


    private int randomColor() {
        return Color.argb(
                0xff,
                (int) (mRandom.nextDouble() * 255),
                (int) (mRandom.nextDouble() * 255),
                (int) (mRandom.nextDouble() * 255)
        );
    }

}