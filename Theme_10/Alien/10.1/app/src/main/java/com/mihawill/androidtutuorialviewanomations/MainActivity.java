package com.mihawill.androidtutuorialviewanomations;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Toast;

import java.lang.annotation.Annotation;

public class MainActivity extends AppCompatActivity {

    private View mFirstView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mFirstView = findViewById(R.id.view_first);
        mFirstView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this,"Click", Toast.LENGTH_SHORT).show();
            }
        });

        findViewById(R.id.view_xml).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFirstView.startAnimation(AnimationUtils.loadAnimation(MainActivity.this, R.anim.demo_animation));
            }
        });

        final AnimationSet animationSet = new AnimationSet(true);
        animationSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animationSet.setFillAfter(true);

        final RotateAnimation rotate = new RotateAnimation(
                0,180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,0.5f);
        rotate.setDuration(500);
        animationSet.addAnimation(rotate);

        final TranslateAnimation translate = new TranslateAnimation(
                Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 100,
                Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 100);
        translate.setDuration(500);
        animationSet.addAnimation(translate);

        final AlphaAnimation alpha = new AlphaAnimation(1,0);
        alpha.setDuration(1000);
        animationSet.addAnimation(alpha);


        findViewById(R.id.view_code).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.view_second).startAnimation(animationSet);
            }
        });
    }
}
