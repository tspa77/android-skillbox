package com.example.androidtutoriallayoutanimations;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

	private final Random mRandom = new Random();

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		final ViewGroup list = (ViewGroup) findViewById(R.id.view_list);

		findViewById(R.id.view_add).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(final View v) {
				final ViewGroup.LayoutParams layoutParams =
						new ViewGroup.LayoutParams(100, ViewGroup.LayoutParams.MATCH_PARENT);
				final int randomColor = Color.argb(
						0xff,
						(int) (mRandom.nextDouble() * 255),
						(int) (mRandom.nextDouble() * 255),
						(int) (mRandom.nextDouble() * 255)
				);
				final View view = new View(MainActivity.this);
				view.setLayoutParams(layoutParams);
				view.setBackgroundColor(randomColor);
				list.addView(view);
			}
		});

		findViewById(R.id.view_remove).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(final View v) {
				final int childCount = list.getChildCount();
				if (childCount > 0) {
					list.removeViewAt((int) (mRandom.nextDouble() * childCount));
				}
			}
		});
	}
}
