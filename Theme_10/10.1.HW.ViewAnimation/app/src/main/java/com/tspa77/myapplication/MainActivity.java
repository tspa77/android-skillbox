package com.tspa77.myapplication;

import android.support.graphics.drawable.AnimationUtilsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private View view_1, view_2;
    private Button buttonXml, buttonCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        view_1 = findViewById(R.id.view_1);
        view_2 = findViewById(R.id.view_2);
        buttonCode = findViewById(R.id.buttonCode);
        buttonXml = findViewById(R.id.buttonXml);

        buttonXml.setOnClickListener(v -> {
            view_1.startAnimation((AnimationUtils.loadAnimation(MainActivity.this, R.anim.demo_animation)));
        });


        final AnimationSet animationSet = new AnimationSet(true);
        animationSet.setInterpolator(new AccelerateDecelerateInterpolator());
        animationSet.setFillAfter(true);

        final RotateAnimation rotate = new RotateAnimation(
                0, -360,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(1000);
        animationSet.addAnimation(rotate);

        final TranslateAnimation translate = new TranslateAnimation(
                Animation.ABSOLUTE, 0, Animation.ABSOLUTE, -500,
                Animation.ABSOLUTE, 0, Animation.ABSOLUTE, 500
        );
        translate.setDuration(1000);
        animationSet.addAnimation(translate);

        final AlphaAnimation alpha = new AlphaAnimation(1f, 0f);
        alpha.setDuration(1000);
        animationSet.addAnimation(alpha);

        buttonCode.setOnClickListener(v -> {
            view_2.startAnimation(animationSet);
        });


    }
}
