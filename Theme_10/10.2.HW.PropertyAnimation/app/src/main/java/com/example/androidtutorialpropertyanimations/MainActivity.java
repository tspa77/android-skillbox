package com.example.androidtutorialpropertyanimations;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		final View firstView = findViewById(R.id.view_first);
		firstView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(final View v) {
				Toast.makeText(MainActivity.this, "Click", Toast.LENGTH_SHORT).show();
			}
		});

		findViewById(R.id.view_xml).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(final View v) {

				final AnimatorSet animatorSet =
						(AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.demo_animation);
				animatorSet.setTarget(firstView);
				animatorSet.start();

			}
		});

		findViewById(R.id.view_code).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(final View v) {
				final View secondView = findViewById(R.id.view_second);

				final ObjectAnimator rotate = ObjectAnimator.ofFloat(secondView, "rotation", 0, 180);
				rotate.setDuration(500);

				final ObjectAnimator translateX = ObjectAnimator.ofFloat(secondView, "translationX", 0, 100);
				translateX.setDuration(500);

				final ObjectAnimator translateY = ObjectAnimator.ofFloat(secondView, "translationY", 0, 100);
				translateY.setDuration(500);

				final ObjectAnimator alpha = ObjectAnimator.ofFloat(secondView, "alpha", 1f, 0f);
				translateY.setDuration(500);

				final AnimatorSet animatorSet = new AnimatorSet();
				animatorSet.playTogether(rotate, translateX, translateY, alpha);
				animatorSet.start();
			}
		});
	}
}
