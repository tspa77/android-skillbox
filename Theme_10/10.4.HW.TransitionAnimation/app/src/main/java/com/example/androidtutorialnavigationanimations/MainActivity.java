package com.example.androidtutorialnavigationanimations;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements PageFragment.Listener {

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager()
					.beginTransaction()
					.replace(R.id.view_page_container, PageFragment.newInstance(0))
					.commit();
		}
	}

	@Override
	public void onOpenPage(final int number) {
		getSupportFragmentManager()
				.beginTransaction()
				.addToBackStack(null)
				.setCustomAnimations(R.anim.in, R.anim.out, R.anim.in, R.anim.out)
				.replace(R.id.view_page_container, PageFragment.newInstance(number))
				.commit();
	}
}
