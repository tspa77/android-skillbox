package com.tspa77.clock;

import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    public static final int TICK_DELAY_MILLIS = 250;
    private SimpleDateFormat mTimeFormat = new SimpleDateFormat("dd:MM:YYYY HH:mm:ss", Locale.US);
    private Handler mHandler = new Handler();
    private TextView mTimeTextView;
    private Runnable mTickRoutine = new Runnable() {
        @Override
        public void run() {
            mTimeTextView.setText(mTimeFormat.format(Calendar.getInstance().getTime()));
            mHandler.postDelayed(this,TICK_DELAY_MILLIS);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTimeTextView = findViewById(R.id.view_time);
        mTickRoutine.run();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacks(mTickRoutine);
        mTimeTextView = null;
    }
}
