package com.tspa77.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DetailActivity extends Activity {

    TextView textViewCard;
    ImageView imageViewFonCard;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        textViewCard = findViewById(R.id.textViewCard);
        imageViewFonCard = findViewById(R.id.imageViewFonCard);

        textViewCard.setOnClickListener(v -> {
            addNewView();
        });

        imageViewFonCard.setOnClickListener(v -> {
            addNewView();
        });
    }

    public void addNewView() {
        LinearLayout container = findViewById(R.id.master);
        LayoutInflater inflater = getLayoutInflater();
        View cardLayer = inflater.inflate(R.layout.view_card_new, null);
        container.addView(cardLayer);
    }
}
