//
// Модуль 9, урок 3:  Instrumentation unit tests
//      Добавить в калькулятор операцию умножения и реализовать UI тест к этой операции.
//
// 35987: Валерий Куликов, 79068017667@yandex.ru
// 2018/11/06
//
//  Примечания:
// 		- добавил в [SimpleCalculator] проверку на переполнение. Для API >= 24 есть методы Math, но у нас API 16
// 		- обернул методы в try/catch
// 		- добавил ряд тестов на исключения
// 		- добавил тестирование ресурсных строк вместо hardcoded
// 		- добавил локализацию на русский
// 		- прогнал тесты на английской и русской локализации (англ.эмулятор и русский смарт)
//

package com.example.androidtutorialespressotesting;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private final SimpleCalculator mSimpleCalculator = new SimpleCalculator();

    private TextView mResultTxtVu;
    private EditText mLeftArgTxtVu;
    private EditText mRightArgTxtVu;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mResultTxtVu = findViewById(R.id.view_result);
        mLeftArgTxtVu = findViewById(R.id.view_first_argument);
        mRightArgTxtVu = findViewById(R.id.view_second_argument);
    }

    @SuppressLint("DefaultLocale")
    public void onAddButtonClick(final View view) {
        try {
            mResultTxtVu.setText(String.format("%s%d", getString(R.string.msg_result),
                    mSimpleCalculator.add(getLeftArg(), getRightArg())));
        } catch (Exception e) {
            mResultTxtVu.setText(String.format("%s%s", getString(R.string.msg_error), e.getLocalizedMessage()));
        }
    }

    @SuppressLint("DefaultLocale")
    public void onSubButtonClick(final View view) {
        try {
            mResultTxtVu.setText(String.format("%s%d", getString(R.string.msg_result),
                    mSimpleCalculator.sub(getLeftArg(), getRightArg())));
        } catch (Exception e) {
            mResultTxtVu.setText(String.format("%s%s", getString(R.string.msg_error), e.getLocalizedMessage()));
        }
    }

    @SuppressLint("DefaultLocale")
    public void onMulButtonClick(View view) {
        try {
            mResultTxtVu.setText(String.format("%s%d", getString(R.string.msg_result),
                    mSimpleCalculator.mul(getLeftArg(), getRightArg())));
        } catch (Exception e) {
            mResultTxtVu.setText(String.format("%s%s", getString(R.string.msg_error), e.getLocalizedMessage()));
        }
    }

    private int getLeftArg() {
        return Integer.valueOf(mLeftArgTxtVu.getText().toString());
    }

    private int getRightArg() {
        return Integer.valueOf(mRightArgTxtVu.getText().toString());
    }

}
