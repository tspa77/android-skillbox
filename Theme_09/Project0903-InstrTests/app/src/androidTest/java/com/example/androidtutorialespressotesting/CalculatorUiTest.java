package com.example.androidtutorialespressotesting;

import android.content.res.Resources;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withSubstring;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class CalculatorUiTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    private Resources mRes = getInstrumentation().getTargetContext().getResources();

    @Test
    public void checkUi() {
        onView(withId(R.id.view_first_argument))
                .check(matches(isDisplayed()));

        onView(withId(R.id.view_second_argument))
                .check(matches(isDisplayed()));

        onView(withText(mRes.getString(R.string.title_result_unavailable)))
                .check(matches(isDisplayed()));

        onView(withText(mRes.getString(R.string.title_add)))
                .check(matches(isDisplayed()));

        onView(withText(mRes.getString(R.string.title_sub)))
                .check(matches(isDisplayed()));

        onView(withText(mRes.getString(R.string.title_multi)))
                .check(matches(isDisplayed()));

    }

    @Test
    public void checkCalculation() {
        onView(withId(R.id.view_first_argument))
                .perform(typeText("1"));

        onView(withId(R.id.view_second_argument))
                .perform(typeText("2"));

        onView(withText(mRes.getString(R.string.title_add)))
                .perform(click());

        final String str = String.format("%s%d", mRes.getString(R.string.msg_result), 3);
        onView(withId(R.id.view_result))
                .check(matches(withText(str)));
    }

    @Test
    public void checkMulCalculation() {
        onView(withId(R.id.view_first_argument))
                .perform(typeText("3"));

        onView(withId(R.id.view_second_argument))
                .perform(typeText("4"));

        onView(withText(mRes.getString(R.string.title_multi)))
                .perform(click());

        final String str = String.format("%s%d", mRes.getString(R.string.msg_result), 12);
        onView(withId(R.id.view_result))
                .check(matches(withText(str)));
    }

    @Test
    public void checkMulCalculationException1() {
        onView(withId(R.id.view_first_argument))
                .perform(typeText("3000000000000"));

        onView(withId(R.id.view_second_argument))
                .perform(typeText("4"));

        onView(withText(mRes.getString(R.string.title_multi)))
                .perform(click());

        onView(withId(R.id.view_result))
                .check(matches(withSubstring(mRes.getString(R.string.msg_error))));
    }

    @Test
    public void checkMulCalculationException2() {
        onView(withId(R.id.view_first_argument))
                .perform(typeText("1"));

        onView(withId(R.id.view_second_argument))
                .perform(typeText("4000000000000"));

        onView(withText(mRes.getString(R.string.title_multi)))
                .perform(click());

        onView(withId(R.id.view_result))
                .check(matches(withSubstring(mRes.getString(R.string.msg_error))));
    }

    @Test
    public void checkMulCalculationException3() {
        onView(withId(R.id.view_first_argument))
                .perform(typeText("2147483647"));

        onView(withId(R.id.view_second_argument))
                .perform(typeText("2"));

        onView(withText(mRes.getString(R.string.title_multi)))
                .perform(click());

        onView(withId(R.id.view_result))
                .check(matches(withSubstring(mRes.getString(R.string.msg_error))));
    }




}
