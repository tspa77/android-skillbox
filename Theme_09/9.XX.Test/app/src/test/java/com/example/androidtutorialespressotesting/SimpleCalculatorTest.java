package com.example.androidtutorialespressotesting;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SimpleCalculatorTest {
    public SimpleCalculator mCalculator;

    @Before
    public void init() {
        mCalculator = new SimpleCalculator();
    }

    @Test
    public void testAddition() {
        Assert.assertEquals("Addition failed", 2, mCalculator.add(1, 1));
    }

    @Test
    public void testSubtraction() {
        Assert.assertEquals("Subtraction failed", 0, mCalculator.sub(1, 1));
    }

    @Test
    public void testMultiplication() {
        Assert.assertEquals("Subtraction failed", 42, mCalculator.mult(6, 7));
    }

    @Test
    public void testDivision() {
        Assert.assertEquals("Subtraction failed", 5, mCalculator.div(35, 7));
    }
}
