package com.example.androidtutorialespressotesting;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.filters.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class CalculatorUiTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void checkUi() {
        onView(withId(R.id.view_first_argument))
                .check(matches(isDisplayed()));

        onView(withId(R.id.view_second_argument))
                .check(matches(isDisplayed()));

        onView(withText("Result: N/A"))
                .check(matches(isDisplayed()));

        onView(withText("Add"))
                .check(matches(isDisplayed()));

        onView(withText("Sub"))
                .check(matches(isDisplayed()));

        onView(withText("Mult"))
                .check(matches(isDisplayed()));

        onView(withText("Div"))
                .check(matches(isDisplayed()));
    }

    @Test
    public void checkCalculation() {
        onView(withId(R.id.view_first_argument))
                .perform(typeText("6"));

        onView(withId(R.id.view_second_argument))
                .perform(typeText("3"));


        onView(withText("Add"))
                .perform(click());

        onView(withId(R.id.view_result))
                .check(matches(withText("Result: 9")));


        onView(withText("Sub"))
                .perform(click());

        onView(withId(R.id.view_result))
                .check(matches(withText("Result: 3")));


        onView(withText("Mult"))
                .perform(click());

        onView(withId(R.id.view_result))
                .check(matches(withText("Result: 18")));


        onView(withText("Div"))
                .perform(click());

        onView(withId(R.id.view_result))
                .check(matches(withText("Result: 2")));

    }
}
