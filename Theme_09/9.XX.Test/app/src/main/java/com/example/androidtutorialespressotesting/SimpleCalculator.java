package com.example.androidtutorialespressotesting;

public class SimpleCalculator {

	public int add(final int a, final int b) {
		return a + b;
	}

	public int sub(final int a, final int b) {
		return a - b;
	}

	public int mult(final int a, final int b) {
		return a * b;
	}

	public int div(final int a, final int b) {
		return (int) a / b;
	}
}
