package com.example.androidtutorialespressotesting;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

	private final SimpleCalculator mSimpleCalculator = new SimpleCalculator();

	private TextView mResultTextView;
	private EditText mFirstArgument;
	private EditText mSecondArgument;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mResultTextView = (TextView) findViewById(R.id.view_result);
		mFirstArgument = (EditText) findViewById(R.id.view_first_argument);
		mSecondArgument = (EditText) findViewById(R.id.view_second_argument);
	}

	public void onAddButtonClick(final View view) {
		mResultTextView.setText("Result: " + mSimpleCalculator.add(getFirstArgument(), getSecondArgument()));
	}

	public void onSubButtonClick(final View view) {
		mResultTextView.setText("Result: " + mSimpleCalculator.sub(getFirstArgument(), getSecondArgument()));
	}

	public void onMultButtonClick(View view) {
		mResultTextView.setText("Result: " + mSimpleCalculator.mult(getFirstArgument(), getSecondArgument()));
	}

	public void onDivButtonClick(View view) {
		mResultTextView.setText("Result: " + mSimpleCalculator.div(getFirstArgument(), getSecondArgument()));
	}

	private int getFirstArgument() {
		return Integer.valueOf(mFirstArgument.getText().toString());
	}

	private int getSecondArgument() {
		return Integer.valueOf(mSecondArgument.getText().toString());
	}
}
