// Модуль 9, урок 2: Unit test
//		Добавить в калькулятор операции умножения и деления и написать тесты к ним.
//
// 35987: Валерий Куликов, 79068017667@yandex.ru
// 2018/11/06
//
//  Примечания:
// 		- добавил в [SimpleCalculator] проверку на переполнение. Для API >= 24 есть методы Math, но у нас API 16
// 		- обернул методы в try/catch
// 		- применил параметризацию тестов для автоматизации проверки рядов значений
//

package com.example.androidtutorialespressotesting;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private final SimpleCalculator mSimpleCalculator = new SimpleCalculator();

    private TextView mResultTextView;
    private EditText mLeftArg;
    private EditText mRightArg;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mResultTextView = findViewById(R.id.view_result);
        mLeftArg = findViewById(R.id.view_first_argument);
        mRightArg = findViewById(R.id.view_second_argument);
    }

    @SuppressLint("DefaultLocale")
    public void onAddButtonClick(final View view) {
        try {
            mResultTextView.setText(String.format("%s%d", getString(R.string.msg_result),
                    mSimpleCalculator.add(getLeftArg(), getRightArg())));
        } catch (Exception e) {
            mResultTextView.setText(String.format("%s%s", getString(R.string.msg_error), e.getLocalizedMessage()));
        }
    }

    @SuppressLint("DefaultLocale")
    public void onSubButtonClick(final View view) {
        try {
            mResultTextView.setText(String.format("%s%d", getString(R.string.msg_result),
                    mSimpleCalculator.sub(getLeftArg(), getRightArg())));
        } catch (Exception e) {
            mResultTextView.setText(String.format("%s%s", getString(R.string.msg_error), e.getLocalizedMessage()));
        }
    }

    private int getLeftArg() {
        return Integer.valueOf(mLeftArg.getText().toString());
    }

    private int getRightArg() {
        return Integer.valueOf(mRightArg.getText().toString());
    }
}
