package com.example.androidtutorialespressotesting;

import android.support.test.filters.SmallTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Параметризованные тесты для сложения.
 */
@RunWith(Parameterized.class)
@SmallTest
public class SimpleCalculatorAddTests {

    /**
     * @return {@link Iterable} значения, передаваемые в конструктор для каждого теста в наборе.
     */
    @SuppressWarnings("NumericOverflow")
    @Parameterized.Parameters
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {0, 0, 0},
                {0, -1, -1},
                {-2, 2, 0},
                {Integer.MAX_VALUE, Integer.MIN_VALUE, -1},
                {Integer.MIN_VALUE, Integer.MAX_VALUE, -1}
        });
    }

    private final int mOperandOne;
    private final int mOperandTwo;
    private final int mExpectedResult;

    private SimpleCalculator mCalculator;

    public SimpleCalculatorAddTests(int operandOne, int operandTwo,
                                    int expectedResult) {

        mOperandOne = operandOne;
        mOperandTwo = operandTwo;
        mExpectedResult = expectedResult;
    }

    @Before
    public void setUp() {
        mCalculator = new SimpleCalculator();
    }

    @Test
    public void testSub_TwoNumbers() {
        int resultAdd = mCalculator.add(mOperandOne, mOperandTwo);
        assertThat(resultAdd, is(equalTo(mExpectedResult)));
    }


}