package com.example.androidtutorialespressotesting;

import android.support.test.filters.SmallTest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

/**
 * Параметризованные тесты для исключений в процессе деления.
 */
@RunWith(Parameterized.class)
@SmallTest
public class SimpleCalculatorDivExceptionTests {

    /**
     * @return {@link Iterable} значения, передаваемые в конструктор для каждого теста в наборе.
     */
    @Parameterized.Parameters
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {0, 0},
                {1, 0},
                {-2, 0},
                {Integer.MIN_VALUE, -1},
                {Integer.MAX_VALUE, 0},
                {Integer.MIN_VALUE, 0}
        });
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private final int mOperandOne;
    private final int mOperandTwo;

    private SimpleCalculator mCalculator;

    public SimpleCalculatorDivExceptionTests(int operandOne, int operandTwo) {

        mOperandOne = operandOne;
        mOperandTwo = operandTwo;
    }

    @Before
    public void setUp() {
        mCalculator = new SimpleCalculator();
    }

    @Test
    public void testExceptionDiv_TwoNumbers() {
        thrown.expect(ArithmeticException.class);
        mCalculator.div(mOperandOne, mOperandTwo);
    }

}