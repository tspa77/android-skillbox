package com.example.androidtutorialespressotesting;

import android.support.test.filters.SmallTest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

/**
 * Параметризованные тесты для сложения.
 */
@RunWith(Parameterized.class)
@SmallTest
public class SimpleCalculatorSubExceptionTests {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    /**
     * @return {@link Iterable} значения, передаваемые в конструктор для каждого теста в наборе.
     */
    @SuppressWarnings("NumericOverflow")
    @Parameterized.Parameters
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {Integer.MAX_VALUE, Integer.MIN_VALUE},
                {Integer.MIN_VALUE, Integer.MAX_VALUE}
        });
    }

    private final int mOperandOne;
    private final int mOperandTwo;

    private SimpleCalculator mCalculator;

    public SimpleCalculatorSubExceptionTests(int operandOne, int operandTwo) {

        mOperandOne = operandOne;
        mOperandTwo = operandTwo;
    }

    @Before
    public void setUp() {
        mCalculator = new SimpleCalculator();
    }

    @Test
    public void testExceptionSub_TwoNumbers() {
        thrown.expect(ArithmeticException.class);
        mCalculator.sub(mOperandOne, mOperandTwo);
    }


}