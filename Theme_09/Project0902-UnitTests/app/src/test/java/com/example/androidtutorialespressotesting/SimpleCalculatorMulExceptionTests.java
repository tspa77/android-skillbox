package com.example.androidtutorialespressotesting;

import android.support.test.filters.SmallTest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

/**
 * Параметризованные тесты для исключений в процессе деления.
 */
@RunWith(Parameterized.class)
@SmallTest
public class SimpleCalculatorMulExceptionTests {

    /**
     * @return {@link Iterable} значения, передаваемые в конструктор для каждого теста в наборе.
     */
    @Parameterized.Parameters
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {Integer.MAX_VALUE, Integer.MAX_VALUE},
                {Integer.MIN_VALUE, Integer.MIN_VALUE},
                {Integer.MAX_VALUE, Integer.MIN_VALUE},
                {Integer.MIN_VALUE, Integer.MAX_VALUE}
        });
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private final int mOperandOne;
    private final int mOperandTwo;

    private SimpleCalculator mCalculator;

    public SimpleCalculatorMulExceptionTests(int operandOne, int operandTwo) {

        mOperandOne = operandOne;
        mOperandTwo = operandTwo;
    }

    @Before
    public void setUp() {
        mCalculator = new SimpleCalculator();
    }

    @Test
    public void testExceptionMul_TwoNumbers() {
        thrown.expect(ArithmeticException.class);
        mCalculator.mul(mOperandOne, mOperandTwo);
    }

}