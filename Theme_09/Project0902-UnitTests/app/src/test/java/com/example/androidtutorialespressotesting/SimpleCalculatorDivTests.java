package com.example.androidtutorialespressotesting;

import android.support.test.filters.SmallTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Параметризованные тесты для деления.
 */
@RunWith(Parameterized.class)
@SmallTest
public class SimpleCalculatorDivTests {

    /**
     * @return {@link Iterable} значения, передаваемые в конструктор для каждого теста в наборе.
     */
    @Parameterized.Parameters
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {1, 1, 1},
                {-2, -1, 2},
                {0, Integer.MAX_VALUE, 0},
                {0, Integer.MIN_VALUE, 0},
                {Integer.MAX_VALUE, Integer.MAX_VALUE, 1},
                {Integer.MIN_VALUE, Integer.MIN_VALUE, 1},
                {Integer.MAX_VALUE, Integer.MIN_VALUE, 0},
                {Integer.MIN_VALUE, Integer.MAX_VALUE, -1}
        });
    }

    private final int mOperandOne;
    private final int mOperandTwo;
    private final int mExpectedResult;

    private SimpleCalculator mCalculator;

    public SimpleCalculatorDivTests(int operandOne, int operandTwo,
                                    int expectedResult) {

        mOperandOne = operandOne;
        mOperandTwo = operandTwo;
        mExpectedResult = expectedResult;
    }

    @Before
    public void setUp() {
        mCalculator = new SimpleCalculator();
    }

    @Test
    public void testDiv_TwoNumbers() {
        int resultAdd = mCalculator.div(mOperandOne, mOperandTwo);
        assertThat(resultAdd, is(equalTo(mExpectedResult)));
    }


}