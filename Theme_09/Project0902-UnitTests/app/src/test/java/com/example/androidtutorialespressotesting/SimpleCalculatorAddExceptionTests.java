package com.example.androidtutorialespressotesting;

import android.support.test.filters.SmallTest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Параметризованные тесты для сложения.
 */
@RunWith(Parameterized.class)
@SmallTest
public class SimpleCalculatorAddExceptionTests {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    /**
     * @return {@link Iterable} значения, передаваемые в конструктор для каждого теста в наборе.
     */
    @SuppressWarnings("NumericOverflow")
    @Parameterized.Parameters
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {1, Integer.MAX_VALUE},
                {Integer.MAX_VALUE, 1},
                {-1, Integer.MIN_VALUE},
                {Integer.MIN_VALUE, -1}
        });
    }

    private final int mOperandOne;
    private final int mOperandTwo;

    private SimpleCalculator mCalculator;

    public SimpleCalculatorAddExceptionTests(int operandOne, int operandTwo) {

        mOperandOne = operandOne;
        mOperandTwo = operandTwo;
    }

    @Before
    public void setUp() {
        mCalculator = new SimpleCalculator();
    }

    @Test
    public void testExceptionAdd_TwoNumbers() {
        thrown.expect(ArithmeticException.class);
        mCalculator.add(mOperandOne, mOperandTwo);
    }


}