package com.example.androidtutorialespressotesting;

import android.support.test.filters.SmallTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Параметризованные тесты для вычитания.
 */
@RunWith(Parameterized.class)
@SmallTest
public class SimpleCalculatorSubTests {

    /**
     * @return {@link Iterable} значения, передаваемые в конструктор для каждого теста в наборе.
     */
    @SuppressWarnings("NumericOverflow")
    @Parameterized.Parameters
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {0, 0, 0},
                {0, -1, 1},
                {-1, 0, -1},
                {Integer.MAX_VALUE, Integer.MAX_VALUE, 0},
                {Integer.MIN_VALUE, Integer.MIN_VALUE, 0}
        });
    }

    private final int mOperandOne;
    private final int mOperandTwo;
    private final int mExpectedResult;

    private SimpleCalculator mCalculator;

    public SimpleCalculatorSubTests(int operandOne, int operandTwo,
                                    int expectedResult) {

        mOperandOne = operandOne;
        mOperandTwo = operandTwo;
        mExpectedResult = expectedResult;
    }

    @Before
    public void setUp() {
        mCalculator = new SimpleCalculator();
    }

    @Test
    public void testAdd_TwoNumbers() {
        int resultAdd = mCalculator.sub(mOperandOne, mOperandTwo);
        assertThat(resultAdd, is(equalTo(mExpectedResult)));
    }


}