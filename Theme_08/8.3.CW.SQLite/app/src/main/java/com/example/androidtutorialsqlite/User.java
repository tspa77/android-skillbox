package com.example.androidtutorialsqlite;

public class User {
	public static final String TABLE_NAME = "User";
	public static final String COLUMN_NAME_ID = "_id";
	public static final String COLUMN_NAME_FIRSTNAME = "firstName";
}
