package com.example.androidtutorialsqlite;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder> {

	private final LayoutInflater mInflater;

	private Cursor mCursor;

	public UserListAdapter(final Context context) {
		mInflater = LayoutInflater.from(context);
	}

	@Override
	public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
		return new ViewHolder(mInflater.inflate(android.R.layout.simple_list_item_1, parent, false));
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, final int position) {
		mCursor.moveToPosition(position);
		final String name = mCursor.getString(mCursor.getColumnIndex(User.COLUMN_NAME_FIRSTNAME));
		holder.userNameTextView.setText(name);
	}

	@Override
	public int getItemCount() {
		return mCursor == null ? 0 : mCursor.getCount();
	}

	public void setCursor(final Cursor cursor) {
		if (mCursor != null) {
			mCursor.close();
		}
		mCursor = cursor;
		notifyDataSetChanged();
	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		public final TextView userNameTextView;

		public ViewHolder(final View itemView) {
			super(itemView);

			userNameTextView = (TextView) itemView.findViewById(android.R.id.text1);
		}
	}
}
