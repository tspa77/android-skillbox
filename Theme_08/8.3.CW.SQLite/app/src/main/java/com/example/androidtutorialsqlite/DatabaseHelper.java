package com.example.androidtutorialsqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
	private static DatabaseHelper sInstance;

	private static final String NAME = "database.db";
	private static final int VERSION = 1;

	public static void createInstance(final Context context) {
		sInstance = new DatabaseHelper(context);
	}

	public static DatabaseHelper getInstance() {
		return sInstance;
	}

	private DatabaseHelper(final Context context) {
		super(context, NAME, null, VERSION);
	}

	@Override
	public void onCreate(final SQLiteDatabase db) {
		final String query = "CREATE TABLE `" + User.TABLE_NAME + "` (`" + User.COLUMN_NAME_FIRSTNAME + "` VARCHAR , `" + User.COLUMN_NAME_ID + "` INTEGER PRIMARY KEY AUTOINCREMENT )";
		db.execSQL(query);
	}

	@Override
	public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
		// do nothing
	}
}
