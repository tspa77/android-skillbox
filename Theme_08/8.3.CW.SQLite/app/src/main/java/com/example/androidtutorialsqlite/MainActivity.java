package com.example.androidtutorialsqlite;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class MainActivity extends AppCompatActivity {

	private final String[] mUserNames = new String[] {
			"Alex",
			"Dmitry",
			"Igor",
			"Anna"
	};

	private final DatabaseHelper mHelper = DatabaseHelper.getInstance();

	private RecyclerView mUserList;
	private UserListAdapter mAdapter;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mUserList = (RecyclerView) findViewById(R.id.view_user_list);
		mAdapter = new UserListAdapter(this);
		mUserList.setAdapter(mAdapter);
		mUserList.setLayoutManager(new LinearLayoutManager(this));

		new SelectTask().execute();
	}

	public void onInsertButtonClick(final View view) {
		new InsertTask().execute();
	}

	public void onDeleteButtonClick(final View view) {
		new DeleteTask().execute();
	}

	private class DeleteTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(final Void... params) {
			mHelper.getWritableDatabase().delete(User.TABLE_NAME, null, null);

			return null;
		}

		@Override
		protected void onPostExecute(final Void aVoid) {
			new SelectTask().execute();
		}
	}

	private class InsertTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(final Void... params) {
			for (final String userName : mUserNames) {
				final ContentValues contentValues = new ContentValues();
				contentValues.put(User.COLUMN_NAME_FIRSTNAME, userName);
				mHelper.getWritableDatabase().insert(User.TABLE_NAME, null, contentValues);
			}

			return null;
		}

		@Override
		protected void onPostExecute(final Void aVoid) {
			new SelectTask().execute();
		}
	}

	private class SelectTask extends AsyncTask<Void, Void, Cursor> {

		@Override
		protected Cursor doInBackground(final Void... params) {
			final Cursor cursor =
					mHelper.getWritableDatabase().query(User.TABLE_NAME, null, null, null, null, null, null);
			cursor.moveToFirst();
			return cursor;
		}

		@Override
		protected void onPostExecute(final Cursor cursor) {
			mAdapter.setCursor(cursor);
		}
	}
}
