package com.example.androidtutorialsqlite;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ArticleListAdapter extends RecyclerView.Adapter<ArticleListAdapter.ViewHolder> {

	private final LayoutInflater mInflater;

	private Cursor mCursor;

	public ArticleListAdapter(final Context context) {
		mInflater = LayoutInflater.from(context);
	}

	@Override
	public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
		return new ViewHolder(mInflater.inflate(android.R.layout.simple_list_item_2, parent, false));
	}

	@Override
	public void onBindViewHolder(final ViewHolder holder, final int position) {
		mCursor.moveToPosition(position);
		final String header = mCursor.getString(mCursor.getColumnIndex(ContractArticle.COLUMN_ARTICLE_HEADER));
		final String body = mCursor.getString(mCursor.getColumnIndex(ContractArticle.COLUMN_ARTICLE_BODY));
		holder.articleHeaderTextView.setText(header);
		holder.articleBodyTextView.setText(body);
	}

	@Override
	public int getItemCount() {
		return mCursor == null ? 0 : mCursor.getCount();
	}

	public void setCursor(final Cursor cursor) {
		if (mCursor != null) {
			mCursor.close();
		}
		mCursor = cursor;
		notifyDataSetChanged();
	}

	public class ViewHolder extends RecyclerView.ViewHolder {
		public final TextView articleHeaderTextView;
		public final TextView articleBodyTextView;

		public ViewHolder(final View itemView) {
			super(itemView);

			articleHeaderTextView = (TextView) itemView.findViewById(android.R.id.text1);
			articleBodyTextView = (TextView) itemView.findViewById(android.R.id.text2);
		}
	}
}
