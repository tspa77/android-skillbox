package com.example.androidtutorialsqlite;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private final String[] mArticleHeaders = new String[]{
            "Telegram не работал несколько минут по всему миру",
            "Как у Huawei P20 Pro, даже больше",
            "«Яндекс» и «Сбербанк» запустили маркетплейс",
            "Samsung до слез осчастливила владельцев смартфонов",
            "Низкий спрос на новинки вынудил Apple вернуть прошлое"
    };

    private final String[] mArticleBodys = new String[]{
            "Пользователи зафиксировали очередной сбой в работе мессенджера Telegram. Он продолжался всего несколько минут, но охватил страны по всему миру.",
            "Камерофон Elephone A5 получил целых пять камер. Несложно догадаться, Huawei P20 Pro он превосходит исключительно по количеству модулей фронтальной камеры",
            "Компании запустили маркетплейс для покупки товаров за рубежом Bringly в тестовом режиме. На Bringly пользователи смогут найти товары из Турции, Южной Кореи, Китая, Германии, Великобритании, Израиля и других стран, перечисляет представитель компании. По его словам, на старте на площадке представлено более четырёх миллионов наименований, включая электронику, одежду, косметику, товары для дома, спортивный инвентарь и прочее. К концу 2019 года компания намерена увеличить число позиций до 50 млн.",
            "Южнокорейская корпорация Samsung создает одним из самых дорогих на рынке смартфонов, да только вот ее продукция, к большому сожалению, крайне редко стоит своей стоимости. Сегодня, 22 ноября 2018 года, данный производитель электроники до слез осчастливил владельцев фирменных мобильных устройств, потому как он пообещал решить проблему, от которой вот уже как более трех месяцев страдают владельцы телефона с ценником свыше 70 тысяч рублей.",
            "Компания Apple возобновила производство смартфонов iPhone X из-за низкого спроса на новые флагманы iPhone XS и XS Max, сообщает The Wall Street Journal. По данным издания, решение начать вновь продавать крайне успешный iPhone X связано с тем, что анонсированные в сентябре iPhone XS и iPhone XS не показали ожидавшихся от них результатов."
    };

    private final DatabaseHelper mHelper = DatabaseHelper.getInstance();

    private RecyclerView mArticleRV;
    private ArticleListAdapter mAdapter;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mArticleRV = (RecyclerView) findViewById(R.id.view_article_header_rv);
        mAdapter = new ArticleListAdapter(this);
        mArticleRV.setAdapter(mAdapter);
        mArticleRV.setLayoutManager(new LinearLayoutManager(this));

        new SelectTask().execute();
    }

    public void onInsertButtonClick(final View view) {
        new InsertTask().execute();
    }

    public void onDeleteButtonClick(final View view) {
        new DeleteTask().execute();
    }

    private class DeleteTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(final Void... params) {
            mHelper.getWritableDatabase().delete(ContractArticle.TABLE_ARTICLE, null, null);

            return null;
        }

        @Override
        protected void onPostExecute(final Void aVoid) {
            new SelectTask().execute();
        }
    }

    private class InsertTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(final Void... params) {
            for (int i = 0; i < mArticleHeaders.length; i++) {
                final ContentValues contentValues = new ContentValues();
                contentValues.put(ContractArticle.COLUMN_ARTICLE_HEADER, mArticleHeaders[i]);
                contentValues.put(ContractArticle.COLUMN_ARTICLE_BODY, mArticleBodys[i]);
                mHelper.getWritableDatabase().insert(ContractArticle.TABLE_ARTICLE, null, contentValues);
            }
            return null;
        }

        @Override
        protected void onPostExecute(final Void aVoid) {
            new SelectTask().execute();
        }
    }

    private class SelectTask extends AsyncTask<Void, Void, Cursor> {

        @Override
        protected Cursor doInBackground(final Void... params) {
            final Cursor cursor =
                    mHelper.getWritableDatabase().query(ContractArticle.TABLE_ARTICLE, null, null, null, null, null, null);
            cursor.moveToFirst();
            return cursor;
        }

        @Override
        protected void onPostExecute(final Cursor cursor) {
            mAdapter.setCursor(cursor);
            mArticleRV.scrollToPosition(mAdapter.getItemCount() - 1);
            Toast.makeText(getApplicationContext(), String.valueOf(mAdapter.getItemCount()) + " entries", Toast.LENGTH_SHORT).show();
        }
    }
}
