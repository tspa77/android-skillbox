package com.example.androidtutorialsqlite;

public class ContractArticle {
	public static final String TABLE_ARTICLE = "Article";
	public static final String COLUMN_ARTICLE_ID = "_id";
	public static final String COLUMN_ARTICLE_HEADER = "articleHeader";
	public static final String COLUMN_ARTICLE_BODY = "articleBody";
}
