package com.example.androidtutorialpreferences;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class InfoFragment extends Fragment {
    private static final String SOME_OPTION_KEY = "SOME_OPTION";
    private static final String SECOND_OPTION_KEY = "SECOND_OPTION";
    private static final String LIST_OPTION_KEY = "LIST_OPTION";

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_info, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final TextView textView = view.findViewById(R.id.view_setting_value);
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        final boolean settingValueSome = settings.getBoolean(SOME_OPTION_KEY, false);
        final boolean settingValueSecond = settings.getBoolean(SECOND_OPTION_KEY, false);

        // Получаю значение списка. Тут наверное можно красивее сделать, но я не знаю как.
        final String entryList;
        if (settings.getString(LIST_OPTION_KEY, "none").equals("none")) {
            entryList = "not selected";
        } else {
            final int settingValueSomeList = Integer.valueOf(settings.getString(LIST_OPTION_KEY, "none"));
            entryList = getResources().getStringArray(R.array.entries)[settingValueSomeList - 1];
        }

        textView.setText(String.format("Some option: %s\nSecond option: %s\nList option: %s",
                String.valueOf(settingValueSome), String.valueOf(settingValueSecond), entryList));
    }
}
