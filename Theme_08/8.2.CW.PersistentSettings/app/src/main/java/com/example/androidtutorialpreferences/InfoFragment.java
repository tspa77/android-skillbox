package com.example.androidtutorialpreferences;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class InfoFragment extends Fragment {
	private static final String SOME_OPTION_KEY = "SOME_OPTION";

	@Nullable
	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_info, container, false);
	}

	@Override
	public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		final TextView textView = (TextView) view.findViewById(R.id.view_setting_value);
		final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
		final boolean settingValue = settings.getBoolean(SOME_OPTION_KEY, false);
		textView.setText(String.format("Some option: %s", String.valueOf(settingValue)));
	}
}
