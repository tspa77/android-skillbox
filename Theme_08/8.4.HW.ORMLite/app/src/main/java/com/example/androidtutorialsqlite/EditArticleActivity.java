package com.example.androidtutorialsqlite;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

import static com.example.androidtutorialsqlite.MainActivity.ACTION;
import static com.example.androidtutorialsqlite.MainActivity.ADD;
import static com.example.androidtutorialsqlite.MainActivity.EDIT;
import static com.example.androidtutorialsqlite.MainActivity.ID_ARTICLE;

public class EditArticleActivity extends AppCompatActivity {
    private final DatabaseHelper mHelper = DatabaseHelper.getInstance();

    TextView textViewId;
    EditText editTextHeader, editTextBody;
    Button buttonSaveExit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_article);

        textViewId = (TextView) findViewById(R.id.id_view);
        editTextHeader = (EditText) findViewById(R.id.header_edit);
        editTextBody = (EditText) findViewById(R.id.body_edit);
        buttonSaveExit = (Button) findViewById(R.id.saveExitButton);

        // Проверяем, для чего вызывали активити
        if (getIntent().getStringExtra(ACTION).equals(EDIT)) {
            // Если редактирование - загружаем статью из БД
            new SelectByIdTask().execute();
        }

        buttonSaveExit.setOnClickListener(view -> {
            if (editTextHeader.getText().toString().trim().isEmpty() || editTextBody.getText().toString().trim().isEmpty()){
                Toast.makeText(this, "Все поля должны быть заполнены", Toast.LENGTH_SHORT).show();
                return;
            }
            // Проверяем, для чего вызывали активити
            if (getIntent().getStringExtra(ACTION).equals(EDIT)) {
                // Обновляем запись
                new UpdateTask().execute();
            } else if (getIntent().getStringExtra(ACTION).equals(ADD)) {
                // Добавляем запись
                new AddTask().execute();
            }
        });
    }


    private class SelectByIdTask extends AsyncTask<Void, Void, Article> {
        // Делаем запрос к БД по ID записи
        @Override
        protected Article doInBackground(Void... voids) {
            long iD = getIntent().getLongExtra(ID_ARTICLE, 0);
            try {
                final Dao<Article, Long> dao = mHelper.getDao(Article.class);
                return dao.queryForId(iD);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        protected void onPostExecute(Article articles) {
            // Заполняем данными поля активити
            textViewId.setText(String.valueOf(articles.getmId()));
            editTextHeader.setText(String.valueOf(articles.getmArticleHeader()));
            editTextBody.setText(String.valueOf(articles.getmArticleBody()));
        }
    }


    private class UpdateTask extends AsyncTask<Void, Void, Void> {
        // Обновляем запись в БД
        @Override
        protected Void doInBackground(final Void... params) {
            try {
                final Dao<Article, Long> dao = mHelper.getDao(Article.class);
                long iD = getIntent().getLongExtra(ID_ARTICLE, 0);
                Article article = dao.queryForId(iD);
                article.setmArticleHeader(editTextHeader.getText().toString());
                article.setmArticleBody(editTextBody.getText().toString());
                dao.update(article);
                return null;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        protected void onPostExecute(final Void aVoid) {
            setResult(RESULT_OK);
            finish();
        }
    }

    private class AddTask extends AsyncTask<Void, Void, Void> {
        // Удаляет текущую запись из таблицы
        @Override
        protected Void doInBackground(final Void... params) {
            try {
                final Dao<Article, Long> dao = mHelper.getDao(Article.class);
                Article article = new Article();
                article.setmArticleHeader(editTextHeader.getText().toString());
                article.setmArticleBody(editTextBody.getText().toString());
                dao.create(article);
                return null;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        protected void onPostExecute(final Void aVoid) {
            setResult(RESULT_OK);
            finish();
        }
    }


}
