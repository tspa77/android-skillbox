package com.example.androidtutorialsqlite;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class ArticleListAdapter extends RecyclerView.Adapter<ArticleListAdapter.ViewHolder> {

    private final LayoutInflater mInflater;
    private List<Article> mData;
    private int[] articleID = {-1, -1}; // Массив для хранения позиции статьи в списке и её ID в БД
    private View previousView;


    public ArticleListAdapter(final Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public int[] getIdSelectedItem() {
        return articleID;
    }


    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        ViewHolder holder = new ViewHolder(mInflater.inflate(android.R.layout.simple_list_item_2, parent, false));

        // Обработчик клика по итемам в RecyclerView
        holder.itemView.setOnClickListener(v -> {

            // Сохраняем позицию адаптера (номер статьи в отображаемом списке)
            articleID[0] = holder.getAdapterPosition();
            if (articleID[0] != RecyclerView.NO_POSITION) {
                // Сохраняем ID BD (номер записи в БД соотвествующий статье)
                articleID[1] = (int) mData.get(articleID[0]).getmId();
                Toast.makeText(parent.getContext(), "Номер в списке: " + articleID[0] + "; Номер в базе: " + articleID[1] + "  ", Toast.LENGTH_SHORT).show(); //+ String.valueOf(holder.itemView)

                // Выделение
                if (((ColorDrawable) holder.itemView.getBackground()).getColor() == Color.TRANSPARENT)
                    holder.itemView.setBackgroundColor(Color.LTGRAY);
                else
                    holder.itemView.setBackgroundColor(Color.TRANSPARENT);

                // Снимаем выделение с прошлой вьюхи (если оно было)
                if ((previousView != null) && (previousView != holder.itemView)) {
                    previousView.setBackgroundColor(Color.TRANSPARENT);
                }
                // и берём ссылку на текущую, для следующего unselect'а
                previousView = holder.itemView;

            } else {
                Toast.makeText(parent.getContext(), "NO_POSITION", Toast.LENGTH_SHORT).show();
            }
        });
        return holder;
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.articleHeaderTextView.setText(mData.get(position).getmArticleHeader());
        holder.articleBodyTextView.setText(mData.get(position).getmArticleBody());

        // Проверяем, если есть выделенная статья - подкрашиваем. Иначе - прозрачный фон
        if (position == articleID[0])
            holder.itemView.setBackgroundColor(Color.LTGRAY);
        else
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    public void setData(final List<Article> data) {
        mData = data;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView articleHeaderTextView;
        public final TextView articleBodyTextView;

        public ViewHolder(final View itemView) {
            super(itemView);
            articleHeaderTextView = (TextView) itemView.findViewById(android.R.id.text1);
            articleBodyTextView = (TextView) itemView.findViewById(android.R.id.text2);
        }
    }
}
