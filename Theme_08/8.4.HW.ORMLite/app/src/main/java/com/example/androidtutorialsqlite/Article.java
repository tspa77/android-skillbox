package com.example.androidtutorialsqlite;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = Article.TABLE_ARTICLE)
public class Article {
    public static final String TABLE_ARTICLE = "Article";

    public static final String COLUMN_ARTICLE_ID = "_id";
    public static final String COLUMN_ARTICLE_HEADER = "articleHeader";
    public static final String COLUMN_ARTICLE_BODY = "articleBody";

    @DatabaseField(generatedId = true,columnName = COLUMN_ARTICLE_ID)
    private long mId;

    @DatabaseField(columnName = COLUMN_ARTICLE_HEADER)
    private String mArticleHeader;

    @DatabaseField(columnName = COLUMN_ARTICLE_BODY)
    private String mArticleBody;

    public long getmId() {
        return mId;
    }

    public void setmId(final long mId) {
        this.mId = mId;
    }

    public String getmArticleHeader() {
        return mArticleHeader;
    }

    public void setmArticleHeader(final String mArticleHeader) {
        this.mArticleHeader = mArticleHeader;
    }

    public String getmArticleBody() {
        return mArticleBody;
    }

    public void setmArticleBody(final String mArticleBody) {
        this.mArticleBody = mArticleBody;
    }
}
