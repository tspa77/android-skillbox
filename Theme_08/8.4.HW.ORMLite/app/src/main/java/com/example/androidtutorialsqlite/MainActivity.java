package com.example.androidtutorialsqlite;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_EDIT = 42;
    public static final int REQUEST_CODE_ADD = 84;
    public static final String ID_ARTICLE = "idArticle";
    public static final String ACTION = "action";
    public static final String EDIT = "edit";
    public static final String ADD = "add";


    private final String[] mArticleHeaders = new String[]{
            "Telegram не работал несколько минут по всему миру",
            "Как у Huawei P20 Pro, даже больше",
            "«Яндекс» и «Сбербанк» запустили маркетплейс",
            "Samsung до слез осчастливила владельцев смартфонов",
            "Низкий спрос на новинки вынудил Apple вернуть прошлое"
    };

    private final String[] mArticleBodys = new String[]{
            "Пользователи зафиксировали очередной сбой в работе мессенджера Telegram. Он продолжался всего несколько минут, но охватил страны по всему миру.",
            "Камерофон Elephone A5 получил целых пять камер. Несложно догадаться, Huawei P20 Pro он превосходит исключительно по количеству модулей фронтальной камеры",
            "Компании запустили маркетплейс для покупки товаров за рубежом Bringly в тестовом режиме. На Bringly пользователи смогут найти товары из Турции, Южной Кореи, Китая, Германии, Великобритании, Израиля и других стран, перечисляет представитель компании. По его словам, на старте на площадке представлено более четырёх миллионов наименований, включая электронику, одежду, косметику, товары для дома, спортивный инвентарь и прочее. К концу 2019 года компания намерена увеличить число позиций до 50 млн.",
            "Южнокорейская корпорация Samsung создает одним из самых дорогих на рынке смартфонов, да только вот ее продукция, к большому сожалению, крайне редко стоит своей стоимости. Сегодня, 22 ноября 2018 года, данный производитель электроники до слез осчастливил владельцев фирменных мобильных устройств, потому как он пообещал решить проблему, от которой вот уже как более трех месяцев страдают владельцы телефона с ценником свыше 70 тысяч рублей.",
            "Компания Apple возобновила производство смартфонов iPhone X из-за низкого спроса на новые флагманы iPhone XS и XS Max, сообщает The Wall Street Journal. По данным издания, решение начать вновь продавать крайне успешный iPhone X связано с тем, что анонсированные в сентябре iPhone XS и iPhone XS не показали ожидавшихся от них результатов."
    };

    private final DatabaseHelper mHelper = DatabaseHelper.getInstance();
    private RecyclerView mArticleRV;
    private ArticleListAdapter mAdapter;
    private int[] articleID = {-1, -1}; // Массив для хранения позиции статьи в списке и её ID в БД

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mArticleRV = (RecyclerView) findViewById(R.id.view_article_rv);
        mAdapter = new ArticleListAdapter(this);
        mArticleRV.setAdapter(mAdapter);
        mArticleRV.setLayoutManager(new LinearLayoutManager(this));
        // Получаем данные из БД
        new SelectAllTask().execute();
    }


    public void onAddBlockButtonClick(final View view) {
        new AddBlockTask().execute();
    }

    public void onClearAllButtonClick(final View view) {
        clearSelect();
        new ClearAllTask().execute();
    }

    public void onAddButtonClick(View view) {
        Intent intent = new Intent(this, EditArticleActivity.class);
        intent.putExtra(ACTION, ADD);
        startActivityForResult(intent, REQUEST_CODE_ADD);
    }

    public void onDeleteButtonClick(View view) {
        if (checkSelectedItem()) {
            clearSelect();
            new DeleteTask().execute();
        }
    }

    public void onEditButtonClick(View view) {
        if (checkSelectedItem()) {
            Toast.makeText(this, String.valueOf(articleID[0]), Toast.LENGTH_SHORT).show();
            // Передаём ID статьи в активити для редактирования
            Intent intent = new Intent(this, EditArticleActivity.class);
            intent.putExtra(ID_ARTICLE, (long) articleID[1]);
            intent.putExtra(ACTION, EDIT);
            startActivityForResult(intent, REQUEST_CODE_EDIT);
        }
    }


    private boolean checkSelectedItem() {
        // Получаем позицию статьи в списке и её ID в БД
        articleID = mAdapter.getIdSelectedItem();
        // Проверяем есть ли выбранная статья в списке
        if (articleID[0] > -1) {
            return true;
        }
        Toast.makeText(this, "Выберите статью", Toast.LENGTH_SHORT).show();
        return false;
    }

    private void clearSelect(){
        // Убираем ссылки на выделенный итем
        articleID[0] = -1;
        articleID[1] = -1;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Проверяем на какой запрос мы отвечаем
        if (requestCode == REQUEST_CODE_EDIT) {
            // Запрос выполнен успешно?
            if (resultCode == RESULT_OK) {
                // Обновляем данные
                new SelectAllTask().execute();
            }
        } else if (requestCode == REQUEST_CODE_ADD) {
            if (resultCode == RESULT_OK) {
                // снимаем выделение со статей, чтоб после добавления фокус уехал на новые статьи
                clearSelect();
                new SelectAllTask().execute();
            }
        }
    }


    private class ClearAllTask extends AsyncTask<Void, Void, Void> {
        // Удаляет все записи из таблицы
        @Override
        protected Void doInBackground(final Void... params) {
            try {
                final Dao<Article, Long> dao = mHelper.getDao(Article.class);
                dao.deleteBuilder().delete();
                return null;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        protected void onPostExecute(final Void aVoid) {
            new SelectAllTask().execute();
        }
    }

    private class DeleteTask extends AsyncTask<Void, Void, Void> {
        // Удаляет текущую запись из таблицы
        @Override
        protected Void doInBackground(final Void... params) {
            try {
                final Dao<Article, Long> dao = mHelper.getDao(Article.class);
                dao.deleteById((long) articleID[1]);
                return null;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        protected void onPostExecute(final Void aVoid) {
            new SelectAllTask().execute();
        }
    }


    private class AddBlockTask extends AsyncTask<Void, Void, Void> {
        // Добавляет готовый блок записей в таблицу
        @Override
        protected Void doInBackground(final Void... params) {
            try {
                final Dao<Article, Long> dao = mHelper.getDao(Article.class);
                for (int i = 0; i < mArticleHeaders.length; i++) {
                    Article article = new Article();
                    article.setmArticleHeader(mArticleHeaders[i]);
                    article.setmArticleBody(mArticleBodys[i]);
                    dao.create(article);
                }
                return null;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        protected void onPostExecute(final Void aVoid) {
            // снимаем выделение со статей, чтоб после добавления фокус уехал на новые статьи
            clearSelect();
            new SelectAllTask().execute();
        }
    }

    private class SelectAllTask extends AsyncTask<Void, Void, List<Article>> {
        // Получаем актуальную выгрузку из БД
        @Override
        protected List<Article> doInBackground(Void... voids) {
            try {
                final Dao<Article, Long> dao = mHelper.getDao(Article.class);
                return dao.queryForAll();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        protected void onPostExecute(List<Article> articles) {
            // Перезаливает данные в адаптере свежей выборкой с БД
            mAdapter.setData(articles);
            // Перемещаем фокус на выделенную статью. Если нет выделения, проверяем, что список не пустой - идём в конец
            if (articleID[0] > -1) {
                mArticleRV.smoothScrollToPosition(articleID[0]);
            } else if (mAdapter.getItemCount() > 0) {
                //
                mArticleRV.smoothScrollToPosition(mAdapter.getItemCount() - 1);
            }
            Toast.makeText(getApplicationContext(), String.valueOf(mAdapter.getItemCount()) + " entries", Toast.LENGTH_SHORT).show();
        }
    }

}

