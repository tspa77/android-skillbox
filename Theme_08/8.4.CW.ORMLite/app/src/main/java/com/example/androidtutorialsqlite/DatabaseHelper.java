package com.example.androidtutorialsqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
	private static DatabaseHelper sInstance;

	private static final String NAME = "database.db";
	private static final int VERSION = 1;

	public static void createInstance(final Context context) {
		sInstance = new DatabaseHelper(context);
	}

	public static DatabaseHelper getInstance() {
		return sInstance;
	}

	private DatabaseHelper(final Context context) {
		super(context, NAME, null, VERSION);
	}

	@Override
	public void onCreate(final SQLiteDatabase sqLiteDatabase, final ConnectionSource connectionSource) {
		try {
			TableUtils.createTable(connectionSource, User.class);
		} catch (final SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void onUpgrade(final SQLiteDatabase sqLiteDatabase, final ConnectionSource connectionSource, final int i, final int i1) {
		// do nothing
	}
}
