package com.example.androidtutorialsqlite;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = User.TABLE_NAME)
public class User {
	public static final String TABLE_NAME = "User";

	public static final String COLUMN_NAME_ID = "_id";
	public static final String COLUMN_NAME_FIRSTNAME = "firstName";

	@DatabaseField(generatedId = true, columnName = COLUMN_NAME_ID)
	private long mId;
	@DatabaseField(columnName = COLUMN_NAME_FIRSTNAME)
	private String mFirstName;

	public long getId() {
		return mId;
	}

	public void setId(final long id) {
		mId = id;
	}

	public String getFirstName() {
		return mFirstName;
	}

	public void setFirstName(final String firstName) {
		mFirstName = firstName;
	}
}
