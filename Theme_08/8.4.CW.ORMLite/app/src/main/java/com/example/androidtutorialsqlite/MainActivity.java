package com.example.androidtutorialsqlite;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

public class MainActivity extends AppCompatActivity {

	private final String[] mUserNames = new String[] {
			"Alex",
			"Dmitry",
			"Igor",
			"Anna"
	};

	private final DatabaseHelper mHelper = DatabaseHelper.getInstance();

	private RecyclerView mUserList;
	private UserListAdapter mAdapter;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mUserList = (RecyclerView) findViewById(R.id.view_user_list);
		mAdapter = new UserListAdapter(this);
		mUserList.setAdapter(mAdapter);
		mUserList.setLayoutManager(new LinearLayoutManager(this));

		new SelectTask().execute();
	}

	public void onInsertButtonClick(final View view) {
		new InsertTask().execute();
	}

	public void onDeleteButtonClick(final View view) {
		new DeleteTask().execute();
	}

	private class DeleteTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(final Void... params) {
			try {
				final Dao<User, Long> dao = mHelper.getDao(User.class);
				dao.deleteBuilder().delete();
			} catch (final SQLException e) {
				throw new RuntimeException(e);
			}
			return null;
		}

		@Override
		protected void onPostExecute(final Void aVoid) {
			new SelectTask().execute();
		}
	}

	private class InsertTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(final Void... params) {
			try {
				final Dao<User, Long> dao = mHelper.getDao(User.class);

				for (final String userName : mUserNames) {
					final User user = new User();
					user.setFirstName(userName);
					dao.create(user);
				}

				return null;
			} catch (final SQLException e) {
				throw new RuntimeException(e);
			}
		}

		@Override
		protected void onPostExecute(final Void aVoid) {
			new SelectTask().execute();
		}
	}

	private class SelectTask extends AsyncTask<Void, Void, List<User>> {

		@Override
		protected List<User> doInBackground(final Void... params) {
			try {
				final Dao<User, Long> dao = mHelper.getDao(User.class);
				return dao.queryForAll();
			} catch (final SQLException e) {
				throw new RuntimeException(e);
			}
		}

		@Override
		protected void onPostExecute(final List<User> users) {
			mAdapter.setData(users);
		}
	}
}
