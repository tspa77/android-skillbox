package com.example.androidtutorialstorage;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity {
	private static final int PERMISSION_REQUEST_CODE = 123;

	private ImageView mImageView;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mImageView = (ImageView) findViewById(R.id.view_image);

		ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
	}

	/*@Override
	public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
		if (requestCode == PERMISSION_REQUEST_CODE) {
			for (int i = 0; i < permissions.length; i++) {
				if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) && grantResults[i] == PackageManager.PERMISSION_DENIED) {
					throw new RuntimeException("WRITE_EXTERNAL_STORAGE is absolutely required");
				}
			}
		}
	}*/

	public void onLoadButtonClick(final View view) {
		new LoadFromPublicStorageTask().execute();
	}

	private class LoadFromPublicStorageTask extends AsyncTask<Void, Void, Bitmap> {

		@Override
		protected Bitmap doInBackground(final Void... params) {
			try {
				final File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
				final File file = new File(dir, "photo_green.png");
				final InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
				return BitmapFactory.decodeStream(inputStream);
			} catch (final FileNotFoundException e) {
				throw new RuntimeException(e);
			}
		}

		@Override
		protected void onPostExecute(final Bitmap bitmap) {
			mImageView.setImageBitmap(bitmap);
		}
	}

	private class LoadFromPrivateStorageTask extends AsyncTask<Void, Void, Bitmap> {

		@Override
		protected Bitmap doInBackground(final Void... params) {
			try {
				final InputStream inputStream = new BufferedInputStream(openFileInput("photo_purple.png"));
				return BitmapFactory.decodeStream(inputStream);
			} catch (final FileNotFoundException e) {
				throw new RuntimeException(e);
			}
		}

		@Override
		protected void onPostExecute(final Bitmap bitmap) {
			mImageView.setImageBitmap(bitmap);
		}
	}

	private class CopyTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(final Void... params) {
			try {
				final OutputStream outputStream = new BufferedOutputStream(openFileOutput("photo_purple.png",
						Context.MODE_PRIVATE));
				final InputStream inputStream = new BufferedInputStream(getAssets().open("photo_purple.png"));
				final byte[] buffer = new byte[1024];
				int length;
				while ((length = inputStream.read(buffer)) > 0) {
					outputStream.write(buffer, 0, length);
				}
				inputStream.close();
				outputStream.close();
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}

			return null;
		}

		@Override
		protected void onPostExecute(final Void aVoid) {
			Toast.makeText(MainActivity.this, "Done", Toast.LENGTH_SHORT).show();
		}
	}

	private class LoadFromAssetsTask extends AsyncTask<Void, Void, Bitmap> {

		@Override
		protected Bitmap doInBackground(final Void... params) {
			try {
				final InputStream inputStream = getAssets().open("photo_green.png");
				return BitmapFactory.decodeStream(inputStream);
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}
		}

		@Override
		protected void onPostExecute(final Bitmap bitmap) {
			mImageView.setImageBitmap(bitmap);
		}
	}

	private class LoadFromResourcesTask extends AsyncTask<Void, Void, Bitmap> {

		@Override
		protected Bitmap doInBackground(final Void... params) {
			final InputStream inputStream = getResources().openRawResource(R.raw.photo_blue);
			return BitmapFactory.decodeStream(inputStream);
		}

		@Override
		protected void onPostExecute(final Bitmap bitmap) {
			mImageView.setImageBitmap(bitmap);
		}
	}
}
