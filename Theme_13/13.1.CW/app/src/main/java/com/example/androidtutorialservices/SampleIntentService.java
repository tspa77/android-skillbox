package com.example.androidtutorialservices;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

public class SampleIntentService extends IntentService {
	private static final String TAG = "SampleIntentService";

	private final Handler mHandler = new Handler(Looper.getMainLooper());

	public SampleIntentService() {
		super("SampleIntentService");
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Log.d(TAG, "onCreate");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy");
	}

	@Override
	protected void onHandleIntent(final Intent intent) {
		try {
			Thread.sleep(3000);
			mHandler.post(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), "Intent Service: task finished", Toast.LENGTH_SHORT).show();
				}
			});
		} catch (final InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

}
