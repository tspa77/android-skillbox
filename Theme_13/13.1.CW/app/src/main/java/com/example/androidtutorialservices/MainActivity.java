package com.example.androidtutorialservices;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
	private final SimpleDateFormat mDateFormat = new SimpleDateFormat("HH:mm:ss", Locale.US);

	private SampleBoundService mService;
	private boolean mIsServiceBound;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	public void onIntentServiceButtonClick(final View view) {
		startService(new Intent(this, SampleIntentService.class));
	}

	public void onStartedServiceButtonClick(final View view) {
		startService(new Intent(this, SampleStartedService.class));
	}

	public void onBoundServiceButtonClick(final View view) {
		if (mIsServiceBound) {
			Toast.makeText(this, mDateFormat.format(mService.getCurrentTime()), Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	protected void onStart() {
		super.onStart();

		bindService(new Intent(this, SampleBoundService.class), mConnection, Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onStop() {
		super.onStop();

		if (mIsServiceBound) {
			unbindService(mConnection);
			mIsServiceBound = false;
		}
	}

	private final ServiceConnection mConnection = new ServiceConnection() {

		private static final String TAG = "SampleBoundService";

		@Override
		public void onServiceConnected(final ComponentName name, final IBinder service) {
			Log.d(TAG, "onServiceConnected");

			final SampleBoundService.LocalBinder binder = (SampleBoundService.LocalBinder) service;
			mService = binder.getService();
			mIsServiceBound = true;
		}

		@Override
		public void onServiceDisconnected(final ComponentName name) {
			Log.d(TAG, "onServiceDisconnected");

			mIsServiceBound = false;
		}
	};
}
