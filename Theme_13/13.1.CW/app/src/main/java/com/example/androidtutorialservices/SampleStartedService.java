package com.example.androidtutorialservices;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

public class SampleStartedService extends Service {

	private static final String TAG = "SampleStartedService";

	private SampleTask mTask;

	@Nullable
	@Override
	public IBinder onBind(final Intent intent) {
		Log.d(TAG, "onBind");
		return null;
	}

	@Override
	public int onStartCommand(final Intent intent, final int flags, final int startId) {
		Log.d(TAG, "onStartCommand");

		if (mTask == null) {
			mTask = new SampleTask();
			mTask.execute();
		}

		return Service.START_NOT_STICKY;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Log.d(TAG, "onCreate");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy");
	}

	private class SampleTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(final Void... params) {
			try {
				Thread.sleep(3000);
			} catch (final InterruptedException e) {
				throw new RuntimeException(e);
			}

			return null;
		}

		@Override
		protected void onPostExecute(final Void aVoid) {
			Toast.makeText(getApplicationContext(), "Started Service: task finished", Toast.LENGTH_SHORT).show();
			stopSelf();
		}
	}
}
