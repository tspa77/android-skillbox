package com.example.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import static com.example.service.Logger.log;

public class BoundService extends Service {
    private final IBinder binder = new LocalBinder();

    public class LocalBinder extends Binder {
        BoundService getService() {
            log(this);
            return BoundService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        log(this);
        return binder;
    }

    @Override
    public void onCreate() {
        log(this);
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        log(this);
        super.onDestroy();
    }

    public int plus(int firstNumber, int secondNumber) {
        log(this);

        return firstNumber + secondNumber;
    }

    public int minus(int firstNumber, int secondNumber) {
        log(this);

        return firstNumber - secondNumber;
    }
}
