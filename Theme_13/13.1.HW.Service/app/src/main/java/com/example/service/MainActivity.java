package com.example.service;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static com.example.service.Logger.log;
import static com.example.service.Logger.logStatic;

public class MainActivity extends AppCompatActivity {

    private BoundService boundService;
    private boolean isServiceBound;

    private Button buttonPlus, buttonMinus;
    private EditText editText1, editText2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        log(this);

        buttonMinus = findViewById(R.id.button_minus);
        buttonPlus = findViewById(R.id.button_plus);
        editText1 = findViewById(R.id.editText1);
        editText2 = findViewById(R.id.editText2);

        buttonMinus.setOnClickListener(v -> {
            calcDifference();
        });

        buttonPlus.setOnClickListener(v -> {
            calcSum();
        });

    }


    private int getNumber(EditText editText) {
        if (editText.getText().toString().isEmpty()) {
            return 0;
        }
        return Integer.parseInt(String.valueOf(editText.getText()));
    }


    private void calcDifference() {
        int result = boundService.minus(getNumber(editText1), getNumber(editText2));
        Toast.makeText(this, "Dif = " + result, Toast.LENGTH_SHORT).show();
    }


    private void calcSum() {
        int result = boundService.plus(getNumber(editText1), getNumber(editText2));
        Toast.makeText(this, "Sum = " + result, Toast.LENGTH_SHORT).show();

    }


    @Override
    protected void onStart() {
        super.onStart();
        log(this);

        bindService(new Intent(this, BoundService.class), connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        log(this);

        if (isServiceBound) {
            unbindService(connection);
            isServiceBound = false;
        }
    }

    private final ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(final ComponentName name, final IBinder service) {
            log(this);

            final BoundService.LocalBinder binder = (BoundService.LocalBinder) service;
            boundService = binder.getService();
            isServiceBound = true;
        }

        @Override
        public void onServiceDisconnected(final ComponentName name) {
            log(this);

            isServiceBound = false;
        }
    };


}
