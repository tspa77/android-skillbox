package com.example.service;

import android.util.Log;

/**
 * Логгер классов и методов, с поддержкой статических методов
 *
 * @author Павел Цыганков
 * @version 1.2
 */
public class Logger {
    /**
     * Поле идентификтаор для Logcat - пять звёздочек
     */
    private static final String TAG = "*****";

    /**
     * Отправляет сообщение с именем объекта и метода в лог
     * @param object - передать ссылку на вызвающий объект "this"
     */
    public static void log(Object object) {
        Log.d(TAG, "\t*****\t [" + object.hashCode() + "]\t\t"
                + object.getClass().getSimpleName() + "\t-->\t"
                + Thread.currentThread().getStackTrace()[3].getMethodName());
    }

    /**
     * Отправляет сообщение с именем объекта, метода и необходимым текстом в лог
     * @param object - передать ссылку на вызвающий объект "this"
     * @param msg    - текст, который нужно залогировать
     */
    public static void log(Object object, String msg) {
        Log.d(TAG, "\t*****\t [" + object.hashCode() + "]\t\t"
                + object.getClass().getSimpleName()
                + "\t-->\t"
                + Thread.currentThread().getStackTrace()[3].getMethodName()
                + "\t-->\t"
                + msg
        );
    }


    /**
     * Из статического метода отправляет сообщение с именем класса и метода в лог
     * @param className - передавать "MethodHandles.lookup().lookupClass().toString()"
     */
    public static void logStatic(String className) {
        Log.d(TAG, "\t*****\t [Static class]\t"
                + className.substring(className.lastIndexOf(".") + 1)
                + "\t-->\t"
                + Thread.currentThread().getStackTrace()[3].getMethodName());
    }

    /**
     * Из статического метода отправляет сообщение с именем класса, метода и необходимым текстом в лог
     * @param className - передавать "MethodHandles.lookup().lookupClass().toString()"
     * @param msg       - текст, который нужно залогировать
     */
    public static void logStatic(String className, String msg) {
        Log.d(TAG, "\t*****\t [Static class]\t"
                + className.substring(className.lastIndexOf(".") + 1)
                + "\t-->\t"
                + Thread.currentThread().getStackTrace()[3].getMethodName()
                + "\t-->\t"
                + msg
        );
    }
}
