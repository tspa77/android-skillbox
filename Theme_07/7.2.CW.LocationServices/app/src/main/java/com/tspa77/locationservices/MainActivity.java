package com.tspa77.locationservices;

import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_CODE = 12;

    private TextView mOutput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mOutput = findViewById(R.id.view_output);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == PERMISSION_REQUEST_CODE){
            for (int i = 0; i < permissions.length; i++) {
                if(permissions[i].equals(Manifest.permission.ACCESS_FINE_LOCATION) && grantResults[i] == PackageManager.PERMISSION_DENIED){
                    throw new RuntimeException("ACCESS_FINE_LOCATION is absolutely required");
                }
            }

        }

        startTrackingLocation();
    }

    private void startTrackingLocation() {

    }

    private void printLocation(final Location location){
        if (location!=null){
            mOutput.setText(String.format("Latitude: %f; longitude: %f", location.getLatitude(), location.getLongitude()));
        } else {
            mOutput.setText("Location N/A");
        }



    }
}
