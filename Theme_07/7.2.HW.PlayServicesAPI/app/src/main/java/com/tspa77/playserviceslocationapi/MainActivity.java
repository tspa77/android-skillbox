package com.tspa77.playserviceslocationapi;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayDeque;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final int PERMISSION_REQUEST_CODE = 42;
    private static final int DEQUE_SIZE = 20;    // Размер строк в текствью, 20 из задания
    private static final String TAG_ARRAY_POINTS = "TAG_ARRAY_POINTS";
    private static final String TAG_SYSTEM_MESSAGE = "TAG_SYSTEM_MESSAGE";

    private ArrayDeque<LatLng> coordinateDeque = new ArrayDeque<>(); // очередь для хранения точек

    private TextView mOutput;
    private TextView mSystemMessage;    //вывожу системное сообщение в отдельную вью

    private GoogleApiClient mGoogleApiClient;

    // Код с урока, без изменеий логики
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mOutput = findViewById(R.id.view_output);
        mSystemMessage = findViewById(R.id.view_system_message);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
            startTrackingLocation();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE
            );
        }
    }


    // Код с урока, без изменеий логики
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            for (int i = 0; i < permissions.length; i++) {
                if (permissions[i].equals(Manifest.permission.ACCESS_FINE_LOCATION) && grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    throw new RuntimeException("ACCESS_FINE_LOCATION is absolutely required");
                }
            }
        }
        startTrackingLocation();
    }

    // Код с урока, без изменеий логики
    @SuppressLint("MissingPermission")
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        @SuppressLint("RestrictedApi") LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
    }

    // Код с урока, без изменеий логики
    private void startTrackingLocation() {
        mGoogleApiClient.connect();
    }

    // При изменении координат обновляю спсиок
    @Override
    public void onLocationChanged(Location location) {
        listUpdate(location);
    }

    // Обновление списка (очереди) координат
    private void listUpdate(Location location) {
        if (location != null) {
            // добавляю новую "точку" в список
            coordinateDeque.add(new LatLng(location.getLatitude(), location.getLongitude()));
            // проверяем размер, "выдавливаем" первых, если размер больше заданного
            while (coordinateDeque.size() > DEQUE_SIZE) {
                coordinateDeque.pop();
            }
            // отправляем на печать
            printLocation(coordinateDeque);
            // убираем системное сообщение о недоступности координат, если оно было
            if(!mSystemMessage.getText().toString().isEmpty()){
                mSystemMessage.setText("");
            }
        } else {
            //если нет координат - сообщаем в отдельном окне
            mSystemMessage.setText(getString(R.string.location_na));
        }
    }


    // Итерирует текущий список, объединяет в одну строку и отправляет в текствью
    private void printLocation(ArrayDeque<LatLng> coordinateDeque) {
        // StringBuilder ибо конкатенация строк это зло
        StringBuilder stringBuilder = new StringBuilder();
        // Склеиваем в одну строку
        for (LatLng point : coordinateDeque) {
            stringBuilder.append(String.format("Latitude: %f; longitude: %f \n", point.latitude, point.longitude));
        }
        mOutput.setText(stringBuilder.toString());
    }


    // Сохраняем данные перед поворотом
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(TAG_ARRAY_POINTS, coordinateDeque);
        outState.putString(TAG_SYSTEM_MESSAGE, mSystemMessage.getText().toString());
    }

    // Восстанавливаем данные после поворота
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        coordinateDeque = (ArrayDeque<LatLng>) savedInstanceState.getSerializable(TAG_ARRAY_POINTS);
        mSystemMessage.setText(savedInstanceState.getString(TAG_SYSTEM_MESSAGE));
    }

    // Код с урока, без изменеий логики
    @Override
    public void onConnectionSuspended(int i) {
        // do nothing
    }

    // Код с урока, без изменеий логики
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        throw new RuntimeException("Connection failed");
    }

    // Код с урока, без изменеий логики
    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }
}
