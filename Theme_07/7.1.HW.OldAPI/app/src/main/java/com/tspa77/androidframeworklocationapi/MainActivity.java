package com.tspa77.androidframeworklocationapi;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;


import java.util.ArrayDeque;

public class MainActivity extends AppCompatActivity {
    private static final int PERMISSION_REQUEST_CODE = 42;
    private static final int DEQUE_SIZE = 20;    // Размер строк в текствью, 20 из задания
    private static final String TAG_ARRAY_POINTS = "TAG_ARRAY_POINTS";
    private static final String TAG_SYSTEM_MESSAGE = "TAG_SYSTEM_MESSAGE";

    private ArrayDeque<Point> coordinateDeque = new ArrayDeque<>(); // очередь для хранения точек

    private TextView mOutput;
    private TextView mSystemMessage;    //вывожу системное сообщение в отдельную вью
    private LocationManager mLocationManager;

    // Код с урока, без изменеий логики
    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            listUpdate(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };

    // Код с урока, без изменеий логики
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        mOutput = findViewById(R.id.view_output);
        mSystemMessage = findViewById(R.id.view_system_message);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
            startTrackingLocation();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE
            );
        }
    }

    // Код с урока, без изменеий логики
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            for (int i = 0; i < permissions.length; i++) {
                if (permissions[i].equals(Manifest.permission.ACCESS_FINE_LOCATION) && grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    throw new RuntimeException("ACCESS_FINE_LOCATION is absolutely required");
                }
            }
        }
        startTrackingLocation();
    }

    // Код с урока, без изменеий логики - только редирект на собственный метод
    @SuppressLint("MissingPermission")
    private void startTrackingLocation() {
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mLocationListener);
        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mLocationListener);

        final Location lastKnownGpsLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (lastKnownGpsLocation != null) {
            listUpdate(lastKnownGpsLocation);   // вызываю свой метод обновления списка
        } else {
            final Location lastKnownNetworkLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            listUpdate(lastKnownNetworkLocation);   // вызываю свой метод обновления списка
        }
    }

    // Обновление списка (очереди) координат
    private void listUpdate(Location location) {
        if (location != null) {
            // добавляю новую "точку" в список
            coordinateDeque.add(new Point(location.getLatitude(), location.getLongitude()));
            // проверяем размер, "выдавливаем" первых, если размер больше заданного
            while (coordinateDeque.size() > DEQUE_SIZE) {
                coordinateDeque.pop();
            }
            // отправляем на печать
            printLocation(coordinateDeque);
            // убираем системное сообщение о недоступности координат, если оно было
            if(!mSystemMessage.getText().toString().isEmpty()){
                mSystemMessage.setText("");
            }
        } else {
            //если нет координат - сообщаем в отдельном окне
            mSystemMessage.setText(getString(R.string.location_na));
        }
    }

    // Итерирует текущий список, объединяет в одну строку и отправляет в текствью
    private void printLocation(ArrayDeque<Point> coordinateDeque) {
        // StringBuilder ибо конкатенация строк это зло
        StringBuilder stringBuilder = new StringBuilder();
        // Склеиваем в одну строку
        for (Point point : coordinateDeque) {
            stringBuilder.append(String.format("Latitude: %f; longitude: %f \n", point.getLatitude(), point.getLongitude()));
        }
        mOutput.setText(stringBuilder.toString());
    }


    // Сохраняем данные перед поворотом
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(TAG_ARRAY_POINTS, coordinateDeque);
        outState.putString(TAG_SYSTEM_MESSAGE, mSystemMessage.getText().toString());
    }

    // Восстанавливаем данные после поворота
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        coordinateDeque = (ArrayDeque<Point>) savedInstanceState.getSerializable(TAG_ARRAY_POINTS);
        mSystemMessage.setText(savedInstanceState.getString(TAG_SYSTEM_MESSAGE));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLocationManager.removeUpdates(mLocationListener);
    }
}



