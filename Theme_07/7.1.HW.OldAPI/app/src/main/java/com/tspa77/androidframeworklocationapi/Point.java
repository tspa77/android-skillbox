/** Класс для хранения координат
 * Есть для этого LatLng из 'com.google.android.gms:play-services:12.0.1'
 * Но знающие люди сказали, что не стоит из-за такого пустяка тащить в проект
 * лишнюю библиотеку.
 *
 * */


package com.tspa77.androidframeworklocationapi;

import java.io.Serializable;

public class Point implements Serializable {

    private double latitude;
    private double longitude;

    public Point(double latitude, double longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
