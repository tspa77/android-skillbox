package com.tspa77.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import static android.hardware.Sensor.TYPE_GYROSCOPE;

public class MainActivity extends AppCompatActivity {

    private SensorManager sensorManager;
    private Sensor gyroscopeSensor;

    private TextView textViewX, textViewY, textViewZ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Получаем мэнеджер сенсоров и непосредственно сенсор гироскопа
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        gyroscopeSensor = sensorManager.getDefaultSensor(TYPE_GYROSCOPE);

        textViewX = findViewById(R.id.axisX);
        textViewY = findViewById(R.id.axisY);
        textViewZ = findViewById(R.id.axisZ);
    }

    SensorEventListener listenerGyroscope = new SensorEventListener() {

        // При изменении датчиков обновляем текстовые вью
        @Override
        public void onSensorChanged(SensorEvent event) {
            textViewX.setText(String.valueOf(event.values[0]));
            textViewY.setText(String.valueOf(event.values[1]));
            textViewZ.setText(String.valueOf(event.values[2]));
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    // При активности подписываемся на слушателя
    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(listenerGyroscope, gyroscopeSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    // На паузе отписываемся
    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(listenerGyroscope);
    }
}
