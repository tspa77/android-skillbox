package com.tspa77.myapplication;

import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    final int MIN_SPAN_COUNT = 1;
    final int MED_SPAN_COUNT = 2;
    final int MAX_VER_SPAN_COUNT = 3;
    final int MAX_HOR_SPAN_COUNT = 5;

    private static int spanCount;
    private static ArrayList<String> names;
    private ListAdapter listAdapter;

    RecyclerView recyclerView;
    FloatingActionButton fab_add_span;
    FloatingActionButton fab_remove_span;
    FloatingActionButton fab_add_item;
    FloatingActionButton fab_remove_item;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        names = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            names.add("Item # " + i);
        }

        recyclerView = findViewById(R.id.view_list);
        spanCount = MED_SPAN_COUNT;
        recyclerView.setLayoutManager(new GridLayoutManager(this, spanCount));
        listAdapter = new ListAdapter(this, names);
        recyclerView.setAdapter(listAdapter);

        fab_add_span = findViewById(R.id.fab_add_span);
        fab_remove_span = findViewById(R.id.fab_remove_span);
        fab_add_item = findViewById(R.id.fab_add_item);
        fab_remove_item = findViewById(R.id.fab_remove_item);


        fab_add_span.setOnClickListener(v -> {
            addSpan();
        });

        fab_remove_span.setOnClickListener(v -> {
            removeSpan();
        });

        fab_add_item.setOnClickListener(v -> {
            addItem();
        });

        fab_remove_item.setOnClickListener(v -> {
            removeItem();
        });

    }

    private void addItem(){
        names.add("Item # " + names.size());
        updateAdapter();
    }

    private void removeItem(){
        if(names.size()>0){
            names.remove(names.size() - 1);
            updateAdapter();
        }
    }

    private void updateAdapter(){
        listAdapter.notifyDataSetChanged();
        recyclerView.scrollToPosition(listAdapter.getItemCount() - 1);
    }


    private void addSpan() {
        spanCount = spanCount < getMaxSpanCount() ? spanCount + 1 : spanCount;
        updateGrid();
    }

    private void removeSpan() {
        spanCount = spanCount > MIN_SPAN_COUNT ? spanCount - 1 : spanCount;
        updateGrid();
    }

    private void updateGrid() {
        recyclerView.setLayoutManager(new GridLayoutManager(this, spanCount));
    }

    private int getMaxSpanCount() {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
            return MAX_VER_SPAN_COUNT;
        else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            return MAX_HOR_SPAN_COUNT;
        else
            return MAX_VER_SPAN_COUNT;
    }


    private static class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

        private final LayoutInflater mInflater;
        private final ArrayList<String> mData;

        public ListAdapter(final Context context, ArrayList<String> mData) {
            this.mInflater = LayoutInflater.from(context);
            this.mData = mData;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            final View view = mInflater.inflate(android.R.layout.simple_list_item_1, viewGroup, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
            holder.textView.setText(mData.get(i));
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            public TextView textView;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                textView = itemView.findViewById(android.R.id.text1);
            }
        }
    }
}

