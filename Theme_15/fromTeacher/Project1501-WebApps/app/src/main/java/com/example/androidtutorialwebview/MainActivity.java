// Модуль 15, урок 1: О Web Apps и области их применения. WebView
//			Открыть в WebView http://ya.ru
//
// 35987: Валерий Куликов, 79068017667@yandex.ru
// 2018/11/24
//
//  Примечания:
//		- по заданию открыл в WebView http://ya.ru
// 		- добавил [ProgressBar] для оживления загрузки
//

package com.example.androidtutorialwebview;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private final static String URL = "http://ya.ru";

	private WebView mWebView;

	/**
	 * Привязка и настройка вьюх.
	 * Настройка обработчиков {@link WebViewClient}.
	 */
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mWebView = findViewById(R.id.view_web);

        mWebView.setVisibility(View.GONE);

        final ProgressBar progressBar = findViewById(R.id.view_progress);
        progressBar.setVisibility(View.VISIBLE);

		final WebSettings settings = mWebView.getSettings();
		settings.setJavaScriptEnabled(true);

		// Привязка состояний вьюх к событиям WebViewClient
		WebViewClient client =  new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(View.GONE);
                mWebView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Toast.makeText(MainActivity.this, getString(R.string.msg_loading) + url, Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.VISIBLE);
                mWebView.setVisibility(View.GONE);
            }
        };
		mWebView.setWebViewClient(client);

		// Обработка загрузки страницы при пересоздании активити.
		if (savedInstanceState == null) {
			mWebView.loadUrl(URL);
		} else {
			mWebView.restoreState(savedInstanceState);
		}
	}

    /**
     * Сохранение состояния {@link WebView}
     */
	@Override
	protected void onSaveInstanceState(final Bundle outState) {
		super.onSaveInstanceState(outState);
		mWebView.saveState(outState);
	}

    /**
     * Обработка отката по кнопке назад с учетом возможной навигации в {@link WebView}
     */
	@Override
	public void onBackPressed() {
		if (mWebView.canGoBack()) {
			mWebView.goBack();
		} else {
			super.onBackPressed();
		}
	}
}
