package com.gdetotut.simplefacebookclient;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Модуль-хелпер работы с настройками приложения {@link SharedPreferences}.
 * Синглтон.
 */
public class PreferencesModule {

    /**
     * Имя файла для настроек.
     */
    private static final String PREFERENCES_FILENAME = "preferences.dat";

    /**
     * Ключ настройки "загрузка постов впервые"
     */
    private static final String IS_POSTS_LOADED_FIRST_TIME_KEY = "IS_POSTS_LOADED_FIRST_TIME";

	private static PreferencesModule sInstance;

	private final SharedPreferences mSettings;

	public static void createInstance(final Context context) {
		sInstance = new PreferencesModule(context);
	}

	public static PreferencesModule getInstance() {
		return sInstance;
	}

	private PreferencesModule(final Context context) {
		mSettings = context.getSharedPreferences(PREFERENCES_FILENAME, Context.MODE_PRIVATE);
	}

    /**
     * Геттер для данных "загрузка постов впервые?"
     * @return Флаг "загрузка постов впервые"
     */
	public boolean isPostsLoadedFirstTime() {
		return mSettings.getBoolean(IS_POSTS_LOADED_FIRST_TIME_KEY, false);
	}

    /**
     * Сеттер для данных "загрузка постов впервые?".
     * Сохраняет флаг.
     */
	public void setPostsLoadedFirstTime(final boolean postsLoadedFirstTime) {
		mSettings.edit().putBoolean(IS_POSTS_LOADED_FIRST_TIME_KEY, postsLoadedFirstTime).apply();
	}
}
