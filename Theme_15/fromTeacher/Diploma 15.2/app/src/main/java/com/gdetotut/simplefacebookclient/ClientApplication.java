package com.gdetotut.simplefacebookclient;

import android.app.Application;

import com.gdetotut.simplefacebookclient.core.AuthModule;
import com.gdetotut.simplefacebookclient.core.db.DatabaseModule;
import com.gdetotut.simplefacebookclient.core.posts.PostsModule;

/**
 * Класс приложения, расширенный инициализациями синглтонов.
 */
public class ClientApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();

		PreferencesModule.createInstance(this);
		AuthModule.createInstance();
        // Deprecated - теперь инициализация FacebookSdk происходит автоматически
//		FacebookSdk.sdkInitialize(getApplicationContext(), new FacebookSdk.InitializeCallback() {
//
//			@Override
//			public void onInitialized() {
//				AuthModule.getInstance().init();
//			}
//		});
		AuthModule.getInstance().init();
		DatabaseModule.createInstance(this);
		PostsModule.createInstance();
	}
}
