// Diploma:
//
// 35987: Валерий Куликов, 79068017667@yandex.ru
// 2018/11/24
//
//  Примечания:
// 		- вставил свой facebook_app_id
// 		- Поправил версии библиотек
// 		- Поправил переводы строковых ресурсов
// 		- Поправил deprecated в коде
// 		- Сменил setVisibility для FloatingActionButton на show/hide (соответствие новым требованиям)
// 		- Хотел добавить Data Binding, но из-за include в layout проброс данных будет многословным, и овчинка не стоит выделки
// 		- А можно использовать библиотеки типа ButterKnife?
//

package com.gdetotut.simplefacebookclient;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gdetotut.simplefacebookclient.core.AuthModule;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

/**
 * Главная активность приложения.
 */
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        CreatePostDialogFragment.Listener {

    /**
     * Ссылка на модуль авторизации.
     */
    private final AuthModule mAuthModule = AuthModule.getInstance();

    /**
     * Сылка на менеджер логина.
     */
    private final LoginManager mLoginManager = LoginManager.getInstance();

    /**
     * Ссылка на колбек-менеджер Facebook.
     */
    private final CallbackManager mCallbackManager = CallbackManager.Factory.create();

    /**
     * Колбек Facebook для входа.
     */
    private final FacebookCallback<LoginResult> mFacebookCallback = new FacebookCallback<LoginResult>() {

        @Override
        public void onSuccess(final LoginResult loginResult) {
            // do nothing
        }

        @Override
        public void onCancel() {
            // do nothing
        }

        @Override
        public void onError(final FacebookException error) {
            // do nothing
        }
    };

    /**
     * Слушатель модуля авторизации, настраивает UI.
     */
    private final AuthModule.Listener mAuthListener = new AuthModule.Listener() {

        @Override
        public void onStateChanged(final AuthModule.State state) {
            switch (state) {

                // Пока не инициализировано.
                case NOT_INITIALIZED:
                    mUsernameTextView.setVisibility(View.GONE);
                    mNotAuthenticatedTextView.setVisibility(View.VISIBLE);
                    mLogoutMenuItem.setVisible(false);
                    mProgressBar.setVisibility(View.VISIBLE);
                    mFloatingActionButton.hide();
                    mHeaderView.setBackgroundResource(R.drawable.side_nav_bar);
                    break;

                // Не авторизован
                case NOT_AUTHENTICATED:
                    mUsernameTextView.setVisibility(View.GONE);
                    mNotAuthenticatedTextView.setVisibility(View.VISIBLE);
                    mLogoutMenuItem.setVisible(false);
                    mProgressBar.setVisibility(View.GONE);
                    mFloatingActionButton.hide();
                    mHeaderView.setBackgroundResource(R.drawable.side_nav_bar);
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.view_content_container, new LoginFragment())
                            .commit();
                    break;

				// Авторизован
                case AUTHENTICATED:
                    mUsernameTextView.setVisibility(View.VISIBLE);
                    mNotAuthenticatedTextView.setVisibility(View.GONE);
                    mLogoutMenuItem.setVisible(true);
                    mProgressBar.setVisibility(View.GONE);
                    mFloatingActionButton.show();
                    mHeaderView.setBackgroundResource(R.drawable.side_nav_bar_auth);
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.view_content_container, new PostsFragment())
                            .commit();
                    break;
            }
        }
    };

    private DrawerLayout mDrawerLayout;
    private TextView mUsernameTextView;
    private TextView mNotAuthenticatedTextView;
    private MenuItem mLogoutMenuItem;
    private ProgressBar mProgressBar;
    private ProfileTracker mProfileTracker;
    private FloatingActionButton mFloatingActionButton;
    private View mHeaderView;

    /**
     * Привязка вьюх и обрабочиков, стартовая настройка данных при создании активности.
     */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDrawerLayout = findViewById(R.id.view_drawer_layout);
        mProgressBar = findViewById(R.id.view_progress_bar);

        final Toolbar toolbar = findViewById(R.id.view_toolbar);
        setSupportActionBar(toolbar);

        mFloatingActionButton = findViewById(R.id.view_fab);
        mFloatingActionButton.setOnClickListener(view -> new CreatePostDialogFragment().show(getSupportFragmentManager(),
                "CreatePostDialogFragment"));

        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        final NavigationView navigationView = findViewById(R.id.view_navigation);
        navigationView.setNavigationItemSelectedListener(this);

        mHeaderView = navigationView.getHeaderView(0);
        mUsernameTextView = mHeaderView.findViewById(R.id.view_username);
        mNotAuthenticatedTextView = mHeaderView.findViewById(R.id.view_not_authenticated);
        mLogoutMenuItem = navigationView.getMenu().findItem(R.id.nav_logout);

        mLoginManager.registerCallback(mCallbackManager, mFacebookCallback);
    }

    /**
     * Передача параметров в менеджер колбеков Facebook.
     */
    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Привязка слушателя для модуля авторизации, запуск трекера профайла Facebook.
     */
    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();

        mAuthModule.addListener(mAuthListener);

        mProfileTracker = new ProfileTracker() {

            @Override
            protected void onCurrentProfileChanged(final Profile oldProfile, final Profile currentProfile) {
                onProfileUpdate();
            }
        };
        onProfileUpdate();
    }

    /**
     * Удаление слушателя авторизации, остановка трекера профайлинга Facebook.
     */
    @Override
    protected void onPause() {
        super.onPause();

        mAuthModule.removeListener(mAuthListener);
        mProfileTracker.stopTracking();
    }

    /**
     * Обработка нажатия кнопки "домой" - закрытие шторки навигатора или переход "назад".
     */
    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Реакция на выбор элемента в навигаторе.
     *  Если элемент "выход" - выходим из Facebook аккаунта.
     *  В конце закрытие шторки навигатора.
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull final MenuItem item) {
        final int id = item.getItemId();

        if (id == R.id.nav_logout) {
            mAuthModule.logout();
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Обновление строки профайла именем авторизованного пользователя.
     */
    private void onProfileUpdate() {
        final Profile profile = Profile.getCurrentProfile();
        if (profile != null) {
            mUsernameTextView.setText(String.format("%s %s", profile.getFirstName(), profile.getLastName()));
        }
    }

    /**
     * Обработка колбека листенера - реакция на создание поста.
     * @param owner владелец интерфейса колбека.
     * @param text текст поста.
     */
    @Override
    public void onCreatePost(final CreatePostDialogFragment owner, final String text) {
        // do nothing
    }
}
