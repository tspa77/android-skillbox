package com.example.androidtutorialwebviewjs;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    public static final String TAG_TEXT = "text";

    private final Handler mHandler = new Handler(Looper.getMainLooper());

    private WebView mWebView;

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mWebView = findViewById(R.id.view_web);
        final WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        mWebView.setWebChromeClient(new WebChromeClient() {

//            @Override
//            public boolean onJsAlert(final WebView view, final String url, final String message,
//                                     final JsResult result) {
//                new AlertDialog.Builder(MainActivity.this)
//                        .setTitle("WebApp")
//                        .setMessage(message)
//                        .setPositiveButton(android.R.string.ok, new AlertDialog.OnClickListener() {
//
//                            @Override
//                            public void onClick(final DialogInterface dialog, final int which) {
//                                result.confirm();
//                            }
//                        })
//                        .setCancelable(false)
//                        .create()
//                        .show();
//
//                return true;
//            }
        });
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.addJavascriptInterface(new WebAppInterface(), "Android");

        if (savedInstanceState == null) {
            mWebView.loadUrl("file:///android_asset/index.html");
        } else {
            mWebView.restoreState(savedInstanceState);
        }
    }


    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        mWebView.saveState(outState);
    }


    private void evaluateJs(final String jsString) {
        mWebView.evaluateJavascript(jsString, null);
    }


    public class WebAppInterface {

        @JavascriptInterface
        public void showActivity(final String text) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    evaluateJs(text);
                }
            });
            Intent intent = new Intent(getApplicationContext(), SecondActivity.class);
            intent.putExtra(TAG_TEXT, text);
            startActivity(intent);
        }
    }


//    private void showMessageActivity(String text) {
//        Intent intent = new Intent(this, SecondActivity.class);
//        intent.putExtra(TAG_TEXT, text);
//        startActivity(intent);
//    }
}
