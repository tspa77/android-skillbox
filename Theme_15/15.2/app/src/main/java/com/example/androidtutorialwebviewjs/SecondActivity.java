package com.example.androidtutorialwebviewjs;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.androidtutorialwebviewjs.databinding.ActivitySecondBinding;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        ActivitySecondBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_second);

        Intent intent = getIntent();
        String text = intent.getStringExtra(MainActivity.TAG_TEXT);
        binding.setText(text);
    }
}
