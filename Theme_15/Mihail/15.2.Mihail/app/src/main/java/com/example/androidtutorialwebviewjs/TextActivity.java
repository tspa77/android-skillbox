package com.example.androidtutorialwebviewjs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class TextActivity extends AppCompatActivity {

    private static final String TOAST_TEXT = "OtherActivity.TOAST";
    private TextView mTextView;
    private String mTextFromJS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text);

        mTextView = (TextView) findViewById(R.id.view_text);
        mTextFromJS = getIntent().getStringExtra(MainActivity.PARAMETER_KEY);
        mTextView.setText(mTextFromJS);
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(TOAST_TEXT, mTextFromJS);
    }
}
