package com.example.androidtutorialwebviewjs;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.androidtutorialwebviewjs.databinding.ActivityMessageBinding;

/**
 * Класс отображения строки из скрипта, передаваемой через интент.
 */
public class MessageActivity extends AppCompatActivity {

    public static final String EXTRA_MSG_KEY = "MessageActivity.MSG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMessageBinding binding =  DataBindingUtil.setContentView(this, R.layout.activity_message);
        setTitle(R.string.title_message_activity);
        String text = getIntent().getExtras().getString(EXTRA_MSG_KEY, getString(R.string.msg_empty_string));
        binding.setText(text);
    }
}
