// Модуль 15, урок 2: Простейшее взаимодействие с Java-кодом
//          По нажатию кнопки в html открывалась Activity с текстом, заданным в html.
//          Состояние при перевороте должно сохраняться.
//
// 35987: Валерий Куликов, 79068017667@yandex.ru
// 2018/11/26
//
//  Примечания:
//      - все по по заданию
//      - использовал data binding вместо findViewById для абстрагирования от конкретной вьюхи
//

package com.example.androidtutorialwebviewjs;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.util.Random;

import static com.example.androidtutorialwebviewjs.MessageActivity.EXTRA_MSG_KEY;

/**
 * Главная активити приложения.
 */
public class MainActivity extends AppCompatActivity {

    private final Random mRandom = new Random();
    private final Handler mHandler = new Handler(Looper.getMainLooper());

    private WebView mWebView;

    /**
     * Настройка вьюх и полей класса.
     * Обработка вызова диалога из скрипта.
     * Обработка пересоздания активности.
     */
    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mWebView = findViewById(R.id.view_web);
        final WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true);

        // по событию создает WebChromeClient создаем нативный диалог.
        mWebView.setWebChromeClient(new WebChromeClient() {

            @Override
            public boolean onJsAlert(final WebView view, final String url, final String message,
                                     final JsResult result) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("WebApp")
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok, new AlertDialog.OnClickListener() {

                            @Override
                            public void onClick(final DialogInterface dialog, final int which) {
                                result.confirm();
                            }
                        })
                        .setCancelable(false)
                        .create()
                        .show();

                return true;
            }
        });
        mWebView.setWebViewClient(new WebViewClient());

        // Назначаем экземпляр WebAppInterface для обработки команд из скрипта
        mWebView.addJavascriptInterface(new WebAppInterface(), "Android");

        // Перезагружаем страницу после пересоздания активности.
        if (savedInstanceState == null) {
            mWebView.loadUrl("file:///android_asset/index.html");
        } else {
            mWebView.restoreState(savedInstanceState);
        }
    }

    /**
     * Сохранение состояния {@link WebView}
     */
    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        mWebView.saveState(outState);
    }

    /**
     * Подготоваливаем строку из скрипта для отображения в {@link WebView} в зависимости от версии API.
     */
    private void evaluateJs(final String jsString) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mWebView.evaluateJavascript(jsString, null);
        } else {
            mWebView.loadUrl("javascript:" + jsString);
        }
    }

    /**
     * Класс реализации интерфейса для работы со скриптом.
     */
    public class WebAppInterface {

        @JavascriptInterface
        public void showToast(final String toast) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    evaluateJs("document.getElementById('msg').innerHTML = '" + mRandom.nextLong() + "'");
                    showMessageActivity(toast);

                }
            });
        }
    }

    /**
     * Запуск экземпляра {@link MessageActivity} для отображения строки из скрипта.
     * @param toast строка из скрипта.
     */
    private void showMessageActivity(String toast) {
        final Intent intent = new Intent(this, MessageActivity.class)
                .putExtra(EXTRA_MSG_KEY, toast);
        startActivity(intent);
    }
}
