// Модуль 11, урок 1: О программных потоках (UI поток), Thread + Handler
//			Выполните рефакторинг кода, связанного с thread и handler (Уберите Runnable в поля)
//
// 35987: Валерий Куликов, 79068017667@yandex.ru
// 2018/11/11
//
//  Примечания:
// 		- [Runnable] убрал в поля и применил lambda-нотацию. Выставил в gradle соответствующие ключи совместимости
// 			с Java-8 для компилятора
//

package com.example.androidtutorialthreads;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private final Handler mHandler = new Handler(Looper.getMainLooper());

    private TextView mTextView;


    private Runnable uiRunnable = () -> {
        Log.d("!!!", "uiRunnable: " + Thread.currentThread().getName());
        mTextView.setText("Hello world");
    };

    private Runnable threadRunnable = () -> {
        Log.d("!!!", "threadRunnable: " + Thread.currentThread().getName());
        mHandler.post(uiRunnable);
    };

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("!!!", "onCreate: " + Thread.currentThread().getName());
        setContentView(R.layout.activity_main);
        mTextView = findViewById(R.id.view_text);
    }

    public void onTestClick(final View view) {
        Log.d("!!!", "onTestClick: " + Thread.currentThread().getName());
        new Thread(threadRunnable).start();
    }
}
