package com.example.androidtutorialthreads;

import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

	private final Handler mHandler = new Handler(Looper.getMainLooper());

	private TextView mTextView;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mTextView = (TextView) findViewById(R.id.view_text);
	}

	public void onTestClick(final View view) {
		Log.d("!!!", "UI thread: " + Thread.currentThread().getName());
		new Thread(new Runnable() {
			@Override
			public void run() {
				Log.d("!!!", "New thread: " + Thread.currentThread().getName());
				mHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						mTextView.setText("Hello world");
					}
				}, 1000);
			}
		}).start();
	}
}
