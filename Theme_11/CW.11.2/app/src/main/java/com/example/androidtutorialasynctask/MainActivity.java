package com.example.androidtutorialasynctask;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

	private static final String RETAINING_FRAGMENT_TAG = "MainActivity.RetainingFragment";
	private static final String TASK_UUID_KEY = "MainActivity.TASK_UUID";

	private RetainingFragment mRetainingFragment;

	private ProgressBar mProgressBar;
	private TextView mResultTextView;
	private ViewGroup mContentLayout;

	private final CalculationTask.Listener mListener = new CalculationTask.Listener() {

		@Override
		public void onProgressChanged(final int progress) {
			mProgressBar.setProgress(progress);
		}

		@SuppressLint("DefaultLocale")
		@Override
		public void onFinished(final int result) {
			updateUi(false);
			mResultTextView.setText(String.format("Result: %d", result));
		}
	};

	private CalculationTask mTask;
	private String mTaskKey;

	@SuppressLint({"DefaultLocale", "SetTextI18n"})
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mProgressBar = (ProgressBar) findViewById(R.id.view_progress_bar);
		mResultTextView = (TextView) findViewById(R.id.view_result);
		mContentLayout = (ViewGroup) findViewById(R.id.view_content);

		if (savedInstanceState == null) {
			mTaskKey = UUID.randomUUID().toString();
			mRetainingFragment = new RetainingFragment();
			getSupportFragmentManager()
					.beginTransaction()
					.add(mRetainingFragment, RETAINING_FRAGMENT_TAG)
					.commit();
		} else {
			mTaskKey = savedInstanceState.getString(TASK_UUID_KEY);
			mRetainingFragment = (RetainingFragment) getSupportFragmentManager()
					.findFragmentByTag(RETAINING_FRAGMENT_TAG);
		}

		mTask = (CalculationTask) mRetainingFragment.get(mTaskKey);
	}

	@Override
	protected void onPause() {
		super.onPause();

		if (mTask != null) {
			mRetainingFragment.put(mTaskKey, mTask);
			mTask.setListener(null);
		}
	}

	@SuppressLint({"DefaultLocale", "SetTextI18n"})
	@Override
	protected void onResume() {
		super.onResume();

		if (mTask != null) {
			if (mTask.getStatus() != AsyncTask.Status.FINISHED) {
				updateUi(true);
				mProgressBar.setProgress(mTask.getProgress());
				mTask.setListener(mListener);
			} else {
				updateUi(false);
				try {
					mResultTextView.setText(String.format("Result: %d", mTask.get()));
				} catch (final InterruptedException | ExecutionException e) {
					throw new RuntimeException(e);
				}
			}
		} else {
			updateUi(false);
			mResultTextView.setText("Result: N/A");
		}
	}

	@Override
	protected void onSaveInstanceState(final Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(TASK_UUID_KEY, mTaskKey);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		if (!isChangingConfigurations()) {
			mRetainingFragment.remove(mTaskKey);
		}
	}

	public void onCalculateClick(final View view) {
		mTask = new CalculationTask();
		mTask.setListener(mListener);
		mProgressBar.setProgress(0);
		mTask.execute();
		updateUi(true);
	}

	private void updateUi(final boolean isInProgress) {
		if (isInProgress) {
			mProgressBar.setVisibility(View.VISIBLE);
			mContentLayout.setVisibility(View.GONE);
		} else {
			mProgressBar.setVisibility(View.GONE);
			mContentLayout.setVisibility(View.VISIBLE);
		}
	}

	private static class CalculationTask extends AsyncTask<Void, Integer, Integer> {

		private Listener mListener;
		private int mProgress;

		@Override
		protected Integer doInBackground(final Void... params) {
			for (int i = 0; i < 100; i++) {
				try {
					Thread.sleep(50);
				} catch (final InterruptedException e) {
					throw new RuntimeException(e);
				}
				publishProgress(i);
			}

			return 42;
		}

		@Override
		protected void onProgressUpdate(final Integer... values) {
			mProgress = values[0];
			if (mListener != null) {
				mListener.onProgressChanged(mProgress);
			}
		}

		@Override
		protected void onPostExecute(final Integer result) {
			if (mListener != null) {
				mListener.onFinished(result);
			}
		}

		public void setListener(final Listener listener) {
			mListener = listener;
		}

		public int getProgress() {
			return mProgress;
		}

		public interface Listener {

			void onProgressChanged(int progress);

			void onFinished(int result);
		}
	}
}
