package com.example.androidtutorialasynctask;

import android.os.AsyncTask;

import java.lang.ref.WeakReference;

public abstract class ProgressableTask extends AsyncTask<Void, Integer, Integer> {

    protected Listener listener;

    public void setListener(final Listener listener) {
        this.listener = listener;
    }

    public abstract int getProgress();

    public interface Listener {
        void onProgressChanged(int progress);
        void onFinished(int result);
    }

}
