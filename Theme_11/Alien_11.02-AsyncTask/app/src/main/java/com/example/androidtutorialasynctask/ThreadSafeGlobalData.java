package com.example.androidtutorialasynctask;

import android.os.AsyncTask;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

public class ThreadSafeGlobalData {

    private static ThreadSafeGlobalData sInstance;

    private final Map<String, ProgressableTask> mStorage = new HashMap<>();

    public static ThreadSafeGlobalData getInstance() {
        if (sInstance == null) {
            synchronized (ThreadSafeGlobalData.class) {
                if (sInstance == null) {
                    sInstance = new ThreadSafeGlobalData();
                }
            }
        }
        return sInstance;
    }

    public void put(final String key, final ProgressableTask value) {
        mStorage.put(key, value);

    }

    public AsyncTask<?, ?, ?> get(final String key) {
        return mStorage.get(key);
    }

    public void remove(final String key) {
        mStorage.remove(key);
    }

    private ThreadSafeGlobalData() {
    }
}
