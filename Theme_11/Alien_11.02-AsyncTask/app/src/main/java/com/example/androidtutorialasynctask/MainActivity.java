// Модуль 11, урок 2: AsyncTask
//			Реализуйте сохранение ссылки на AsyncTask, но не в Retain Fragment, а в Singleton
//
// 35987: Валерий Куликов, 79068017667@yandex.ru
// 2018/11/11
//
//  Примечания:
// 		- Слегка украсил прогресс
// 		- Singleton сделал с ленивой инициализацией + потокобезопасным. Исчезла необходимость
// 			в наследнике [Application]
// 		- Карта в [ThreadSafeGlobalData] хранит не объекты, а максимально обобщенные [ProgressableTask],
//			потому что мы ожидаем от итемов не только интерфейс, но и определенного поведения
//			и чрезмерное обобщение чревато...
//

package com.example.androidtutorialasynctask;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    private static final String TASK_UUID_KEY = "MainActivity.TASK_UUID";

    private ThreadSafeGlobalData mData = ThreadSafeGlobalData.getInstance();

    private ProgressBar mProgressBar;
    private TextView mStepTextView;
    private TextView mResultTextView;
    private ViewGroup mProgressLayout;
    private ViewGroup mContentLayout;

    private final ProgressableTask.Listener mListener = new ProgressableTask.Listener() {

        @SuppressLint("DefaultLocale")
        @Override
        public void onProgressChanged(final int progress) {
            mProgressBar.setProgress(progress);
            mStepTextView.setText(String.format("%d%%", progress));
        }

        @SuppressLint("DefaultLocale")
        @Override
        public void onFinished(final int result) {
            updateUi(false);
            mResultTextView.setText(String.format("Result: %d", result));
        }
    };

    private ProgressableTask mTask;
    private String mTaskKey;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mProgressBar = findViewById(R.id.view_progress_bar);
        mStepTextView = findViewById(R.id.view_progress_step);
        mResultTextView = findViewById(R.id.view_result);
        mProgressLayout = findViewById(R.id.view_progress_layout);
        mContentLayout = findViewById(R.id.view_content);

        if (savedInstanceState == null) {
            mTaskKey = UUID.randomUUID().toString();
        } else {
            mTaskKey = savedInstanceState.getString(TASK_UUID_KEY);
        }

        mTask = (ProgressableTask) mData.get(mTaskKey);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mTask != null) {
            mData.put(mTaskKey, mTask);
            mTask.setListener(null);
        }
    }

    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    @Override
    protected void onResume() {
        super.onResume();

        if (mTask != null) {
            if (mTask.getStatus() != AsyncTask.Status.FINISHED) {
                updateUi(true);
                mProgressBar.setProgress(mTask.getProgress());
                mTask.setListener(mListener);
            } else {
                updateUi(false);
                try {
                    mResultTextView.setText(String.format("Result: %d", mTask.get()));
                } catch (final InterruptedException | ExecutionException e) {
                    throw new RuntimeException(e);
                }
            }
        } else {
            updateUi(false);
            mResultTextView.setText("Result: N/A");
        }
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(TASK_UUID_KEY, mTaskKey);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (!isChangingConfigurations()) {
            mData.remove(mTaskKey);
        }
    }

    public void onCalculateClick(final View view) {
        mTask = new CalculationTask();
        mTask.setListener(mListener);
        mProgressBar.setProgress(0);
        mTask.execute();
        updateUi(true);
    }

    private void updateUi(final boolean isInProgress) {
        if (isInProgress) {
            mProgressLayout.setVisibility(View.VISIBLE);
            mContentLayout.setVisibility(View.GONE);
        } else {
            mProgressLayout.setVisibility(View.GONE);
            mContentLayout.setVisibility(View.VISIBLE);
        }
    }

    private static class CalculationTask extends ProgressableTask {

        private int mProgress;

        @Override
        protected Integer doInBackground(final Void... params) {
            for (int i = 0; i < 100; i++) {
                try {
                    Thread.sleep(50);
                } catch (final InterruptedException e) {
                    throw new RuntimeException(e);
                }
                publishProgress(i);
            }

            return 42;
        }

        @Override
        protected void onProgressUpdate(final Integer... values) {
            mProgress = values[0];
            if (listener != null) {
                listener.onProgressChanged(mProgress);
            }
        }

        @Override
        protected void onPostExecute(final Integer result) {
            if (listener != null) {
                listener.onFinished(result);
            }
        }

        public int getProgress() {
            return mProgress;
        }

    }
}
