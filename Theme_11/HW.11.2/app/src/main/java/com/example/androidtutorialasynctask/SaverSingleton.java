package com.example.androidtutorialasynctask;

import java.util.HashMap;
import java.util.Map;

public final class SaverSingleton {
    private static SaverSingleton sInstance;

    private final Map<String, Object> mStorage = new HashMap<>();

    private SaverSingleton() {
    }


    public static SaverSingleton getInstance() {
        if (sInstance == null) {
            synchronized (SaverSingleton.class) {
                if (sInstance == null) {
                    sInstance = new SaverSingleton();
                }
            }
        }
        return sInstance;
    }

    public void put(final String key, final Object value) {
        mStorage.put(key, value);
    }

    public Object get(final String key) {
        return mStorage.get(key);
    }

    public void remove(final String key) {
        mStorage.remove(key);
    }
}

