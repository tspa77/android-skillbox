package com.example.androidtutorialthreads;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private final Handler mHandler = new Handler(Looper.getMainLooper());
    private TextView mTextView;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("!!!", "onCreate: " + Thread.currentThread().getName());
        setContentView(R.layout.activity_main);

        mTextView = findViewById(R.id.view_text);
    }

    public void onTestClick(final View view) {
        Log.d("!!!", "onTestClick: " + Thread.currentThread().getName());
        mHandler.post(new HelloWriter());
    }

    class HelloWriter implements Runnable {
        @Override
        public void run() {

            Log.d("!!!", "class HelloWriter - run: " + Thread.currentThread().getName());
            mTextView.setText(("Hello world"));
        }
    }
}

