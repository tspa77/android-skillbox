package com.tspa77.cw_intent;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    private static final int TAKE_PHOTO_CODE = 4567;
    private static final String PHOTO_URI_KEY = "MainActivity.PHOTO_URI";

    private ImageView mPhotoImageView;
    private Button mPhotoButton;
    private Uri mPhotoUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPhotoImageView = findViewById(R.id.view_photo);

        mPhotoButton = findViewById(R.id.button_take_photo);

        mPhotoButton.setOnClickListener(v -> {
            mPhotoUri = createPhotoUri();
            final Intent intent = new Intent();
            intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri);

            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());

            startActivityForResult(intent, TAKE_PHOTO_CODE);

        });

        if (savedInstanceState != null) {
            mPhotoUri = savedInstanceState.getParcelable(PHOTO_URI_KEY);
        }

        if (mPhotoUri != null) {
            new LoadPhotoTask().execute(mPhotoUri);
        }

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable(PHOTO_URI_KEY, mPhotoUri);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TAKE_PHOTO_CODE && resultCode == RESULT_OK) {
            new LoadPhotoTask().execute(mPhotoUri);
        }

    }


    private Uri createPhotoUri() {
        final File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        path.mkdirs();
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss_SSS");
        final File photoFile = new File(path, dateFormat.format(Calendar.getInstance().getTime()) + ".jpg");
        return Uri.fromFile(photoFile);

    }

    private class LoadPhotoTask extends AsyncTask<Uri, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(Uri... uris) {
            final Uri photoUri = uris[0];
            return BitmapFactory.decodeFile(photoUri.getPath());
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            mPhotoImageView.setImageBitmap(bitmap);
        }
    }

}
