package com.tspa77.a61hw_intent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText mEditText;
    private Button mButtonNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEditText = findViewById(R.id.editText);
        mButtonNext = findViewById(R.id.buttonNext);

        // Состояние EditText сохранять не нужно, Android сам об этом позаботится

        // Обработчик нажатия на кнопку
        mButtonNext.setOnClickListener(v -> {
            // если в поле не пусто:
            if(!mEditText.getText().toString().trim().isEmpty()){
                Intent intent = new Intent(this, SecontActivity.class);           // создаём интент
                intent.putExtra(SecontActivity.TEXT, mEditText.getText().toString().trim());    // передаём в него текст
                startActivity(intent);                                                          // запускаем вторую активити
            }else{
                // если пусто - показываем предупреждение
                Toast.makeText(this, "Введите данные", Toast.LENGTH_SHORT).show();
            }
        });


    }
}
