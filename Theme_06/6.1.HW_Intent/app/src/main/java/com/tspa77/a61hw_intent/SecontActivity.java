package com.tspa77.a61hw_intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SecontActivity extends AppCompatActivity {
    public static final String TEXT = "text";

    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secont);

        mTextView = findViewById(R.id.textView);

        // Проверки savedInstanceState на налл не нужны, т.к. данная активити вызывается только
        // методом, в котором в бандл данные передаются. Для сохранения состояния при перевороте,
        // соответственно, ничего более не нужно. Каждый раз дёргается имеющийся бандл.


        //Bundle bundle = getIntent().getExtras();
        if (getIntent().getExtras() != null && !getIntent().getExtras().isEmpty()) {
            mTextView.setText(getIntent().getExtras().getString(TEXT));
        }

    }
}
