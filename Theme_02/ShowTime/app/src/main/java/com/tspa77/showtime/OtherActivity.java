package com.tspa77.showtime;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class OtherActivity extends AppCompatActivity {
    private TextView mTextViewTime;
    private Button mButtonEnable;
    private Button mButtonDisable;

    private SimpleDateFormat mTimeFormat = new SimpleDateFormat("dd:MM:YYYY HH:mm:ss", Locale.US);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other);

        mTextViewTime = findViewById(R.id.view_time);
        mButtonEnable = findViewById(R.id.button_enable);
        mButtonDisable = findViewById(R.id.button_disable);

        mButtonEnable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTextViewTime.setText(mTimeFormat.format(Calendar.getInstance().getTime()));

            }
        });

        mButtonDisable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTextViewTime.setText("XX:XX:XX");
            }
        });

    }


}
